// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'app.controllers', 'app.routes', 'app.directives','app.services','ngCordova','ion-floating-menu','ion-google-place','nzToggle','ngAutocomplete','jett.ionic.filter.bar','ionic-modal-select'])

.config(function($ionicConfigProvider, $sceDelegateProvider,$httpProvider){

  $sceDelegateProvider.resourceUrlWhitelist([ 'self','*://www.youtube.com/**', '*://player.vimeo.com/video/**']);

  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
  $httpProvider.defaults.headers.put = {};
  $httpProvider.defaults.headers.patch = {};

 /* $ionicCloudProvider.init({
    "core": {
      "app_id": "cdfcc333"
    }
  });*/



})

.run(function($ionicPlatform,$ionicPopup,$cordovaNetwork,$rootScope,$cordovaFile,loginService,$state) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    if ($cordovaNetwork.isOffline()) {
      alert("No Internet Connection");
      navigator.app.exitApp();
    }
    $cordovaFile.checkFile(cordova.file.dataDirectory, "kaammilega.txt")
    .then(function(success) {
      readfile();
    }, function(error) {
      $state.go('login');
    });

    function readfile() {
      $cordovaFile.readAsText(cordova.file.dataDirectory, "kaammilega.txt")
      .then(function(success) {
        verifyfile(success);
      }, function(error) {
        $state.go('login');
        
      });
    }

    function verifyfile(mail){

      var mailData = {email:mail};
      loginService.verifymail(mailData,
        function(data){
          var res = data.split(":");


          $rootScope.type = res[0];
          $rootScope.id = res[1];
          if(res[0] == 'retailer')
          {
            $state.go('menu.homeRetailer');
          }
          else if(res[0] == 'salesman') {
            $state.go('menu.homeSalesman')
          }
          else{
            $state.go('login')
          }
        }, function(data){
          alert('error verifying mail')
          alert(data);
          $state.go('login')
        })
    }



    var notificationOpenedCallback = function(data) {
      var type = data.notification.payload.additionalData.type;
      // alert(type);
      if(type == "applicant"){
        var status = data.notification.payload.additionalData.status;
        var job_id = data.notification.payload.additionalData.job_id;
        $state.go('allJobDetail',{
          jobId: job_id
        })
      }
      else if(type == "retailer"){
        var aId = data.notification.payload.additionalData.a_id;
        $state.go('applicantDetail',{
          aId: aId,
          vname: 'homeRetailer'
        }) 
      }
    };


    window.plugins.OneSignal
    .startInit("31e759f6-a64f-44ea-81e1-ee32e49d019c")
    .handleNotificationOpened(notificationOpenedCallback)
    .endInit();


    window.plugins.OneSignal.getIds(function(data) {
     var localStorage = window.localStorage;
     localStorage.clear();
     localStorage.setItem("device", data.userId);
     // alert(localStorage.getItem("device"));
   });

    window.plugins.OneSignal.setSubscription(true)
    window.plugins.OneSignal.enableVibrate(true);
    window.plugins.OneSignal.enableSound(true);


    $ionicPlatform.registerBackButtonAction(function(event) {
      if ($state.current.name == "menu.allSalesman" || $state.current.name == "menu.homeRetailer" || $state.current.name == "menu.homeSalesman" || $state.current.name == "login" || $state.current.name == "menu.retailerProfile" || $state.current.name == "menu.retailerShops" || $state.current.name == "menu.salesmanAppliedJobs" || $state.current.name == "menu.salesmanExperience" || $state.current.name == "menu.salesmanProfile" || $state.current.name == "otp" ) {
        $ionicPopup.confirm({
          title: 'System warning',
          template: '<span style="color:#000;text-align: center;">Are you sure you want to exit?</span>',
          okType: 'button-energized'
        }).then(function(res) {
          if (res) {
            ionic.Platform.exitApp();
          }
        })
      }

      else if( $state.current.name == "addExperience"){
        if($rootScope.vname == 'salesmanExperience'){
          $state.go('menu.salesmanExperience');
        }
      }    
      else if($state.current.name == "addShop" || $state.current.name == "shopDetail"){
        $state.go('menu.retailerShops');
      }
      else if($state.current.name == "retailer" || $state.current.name == "salesman"){
        $state.go('login');
      }
      else if($state.current.name == "viewJobs" || $state.current.name == "shopApplicants" || $state.current.name == "postJob"){
        $state.go('shopDetail',{
          sId : $rootScope.sId
        });
      }
      else if($state.current.name == "jobDetail"){
        $state.go('viewJobs',{
          sId : $rootScope.sId
        });
      }

      else if($state.current.name == "postJob"){
       if($rootScope.vname == 'viewJobs'){
        $state.go('viewJobs',{
          sId: $rootScope.sId
        })
      }
      else if($rootScope.vname == 'shopDetail'){
        $state.go('shopDetail',{
          sId: $rootScope.sId
        })
      }
    }

    else if($state.current.name == "allJobDetail"){
     if($rootScope.vname == 'homeSalesman'){
      $state.go('menu.homeSalesman')
    }
    else if($rootScope.vname == 'salesmanAppliedJobs'){
      $state.go('menu.salesmanAppliedJobs') 
    }
    else if($rootScope.vname == 'rejectedApplicant'){
        $state.go('menu.rejectedApplicant')
    } 
  }

  else if($state.current.name == "applicantDetail"){
    if($rootScope.vname == 'homeRetailer'){
      $state.go('menu.homeRetailer')
    }
    else if($rootScope.vname == 'selectedApplicant'){
      $state.go('menu.selectedApplicant')
    }
    else if($rootScope.vname == 'jobDetail'){
      $state.go('jobDetail',{
        jId: $rootScope.jId,
        sId: $rootScope.sId
      })
    }
    else if($rootScope.vname == 'shopApplicants'){
      $state.go('shopApplicants',{
        sId: $rootScope.sId
      })
    }
    else if($rootScope.vname == 'allSalesman'){
      $state.go('menu.allSalesman',{
        sId: $rootScope.sId
      })
    }
  }

  else {
    navigator.app.backHistory();
  }

}, 100);

  }); // platform ready closed
})

/*
  This directive is used to disable the "drag to open" functionality of the Side-Menu
  when you are dragging a Slider component.
  */
  .directive('disableSideMenuDrag', ['$ionicSideMenuDelegate', '$rootScope', function($ionicSideMenuDelegate, $rootScope) {
    return {
      restrict: "A",  
      controller: ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {

        function stopDrag(){
          $ionicSideMenuDelegate.canDragContent(false);
        }

        function allowDrag(){
          $ionicSideMenuDelegate.canDragContent(true);
        }

        $rootScope.$on('$ionicSlides.slideChangeEnd', allowDrag);
        $element.on('touchstart', stopDrag);
        $element.on('touchend', allowDrag);
        $element.on('mousedown', stopDrag);
        $element.on('mouseup', allowDrag);

      }]
    };
  }])

/*
  This directive is used to open regular and dynamic href links inside of inappbrowser.
  */
  .directive('hrefInappbrowser', function() {
    return {
      restrict: 'A',
      replace: false,
      transclude: false,
      link: function(scope, element, attrs) {
        var href = attrs['hrefInappbrowser'];

        attrs.$observe('hrefInappbrowser', function(val){
          href = val;
        });

        element.bind('click', function (event) {

          window.open(href, '_system', 'location=yes');

          event.preventDefault();
          event.stopPropagation();

        });
      }
    };
  });