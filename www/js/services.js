// var baseUrl = 'http://13.126.220.182/api/';	
var baseUrl = 'http://ec2-13-126-48-166.ap-south-1.compute.amazonaws.com/api/';	
// var baseUrl = 'http://localhost:8000/api/';	

angular.module('app.services', ['ngCordova'])
/*
.factory('BlankFactory', [function(){

}])

.service('BlankService', [function(){

}]);*/



.factory('loginService', ['$http',function($http){
	// var baseUrl = 'http://localhost:8000/api/'	 
	var loginService = {};

	loginService.authenticate = function (loginData,onSuccess,onError) {

		$http({
			method: 'POST',
			url: baseUrl+'ui/login',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data:loginData

		}).then(function (response) {
			onSuccess(response.data);
		}, function(response){
			onError(response);
		});
	}

	loginService.verifymail = function(mailData,onSuccess,onError){
		$http({
			method: 'POST',
			url: baseUrl+'ui/verify-mail',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data:mailData

		}).then(function (response) {
			onSuccess(response.data);
		}, function(response){
			onError(response);
		});
	}


	return loginService;
}])

.factory('RetailerService', ['$http',function($http){

	// var baseUrl = 'http://localhost:8000/api/';
	// var baseUrl = 'http://ec2-52-14-198-231.us-east-2.compute.amazonaws.com/api/'	

	var RetailerService = {};


	RetailerService.register = function(retailer,onSuccess,onError){


		$http({
			method : 'POST',
			url : baseUrl+'user',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: retailer
		}).then(function(response){
			console.log("response"+response)
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});

	}

	RetailerService.getRetailerById = function(rId, onSuccess, onError){

		$http({
			method : 'GET',
			url : baseUrl+'user/'+rId
		}).then(function(response){

			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});

	}

	RetailerService.updateRetailer = function(retailerUpdateData, onSuccess, onError){

		$http({
			method : 'POST',
			url : baseUrl+'update-user',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data:retailerUpdateData 
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});


	}





	return RetailerService;
}])

.factory('ApplicantService',['$http',function($http){

	// var baseUrl = 'http://ec2-52-14-198-231.us-east-2.compute.amazonaws.com/api/'	
	// var baseUrl = 'http://localhost:8000/api/'	

	var ApplicantService = {};

	ApplicantService.allApplicant = function(rId,onSuccess,onError){

		$http({
			method : 'GET',
			url : baseUrl+'ui/get-applicant-by-retailer/'+rId
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	ApplicantService.selectedApplicant = function(rId,onSuccess,onError){

		$http({
			method : 'GET',
			url : baseUrl+'ui/get-selected-applicant-by-retailer/'+rId
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	ApplicantService.rejectedApplicant = function(rId,onSuccess,onError){

		$http({
			method : 'GET',
			url : baseUrl+'ui/get-rejected-applicant-by-retailer/'+rId
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	ApplicantService.appliedApplicant = function(rId,onSuccess,onError){

		$http({
			method : 'GET',
			url : baseUrl+'ui/get-applied-applicant-by-retailer/'+rId
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	ApplicantService.getApplicantsByShopId = function(sId,onSuccess,onError){

		$http({
			method : 'GET',
			url : baseUrl+'ui/applicants-shopid/'+sId
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	ApplicantService.getApplicantsByJobId = function(jId,onSuccess,onError){

		$http({
			method : 'GET',
			url : baseUrl+'ui/applicants-jobid/'+jId
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

/*	ApplicantService.getById = function(aId,onSuccess,onError){
		$http({
			method : 'GET',
			url : baseUrl+'applicant/'+aId
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}*/

	ApplicantService.getApplicant = function(applicantData,onSuccess,onError){
		$http({
			method : 'POST',
			url : baseUrl+'ui/applicant-detail',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: applicantData
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	// dipesh code 
	ApplicantService.getSelectedApplicant = function(applicantData,onSuccess,onError){
		$http({
			method : 'POST',
			url : baseUrl+'ui/selected-applicant-detail',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: applicantData
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	ApplicantService.getRejectedApplicant = function(applicantData,onSuccess,onError){
		$http({
			method : 'POST',
			url : baseUrl+'ui/rejected-applicant-detail',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: applicantData
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	ApplicantService.getAppliedApplicant = function(applicantData,onSuccess,onError){
		$http({
			method : 'POST',
			url : baseUrl+'ui/applied-applicant-detail',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: applicantData
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}
	// dipesh code 
	
	ApplicantService.changeStatus = function(statusChangeData,onSuccess,onError){
		$http({
			method : 'POST',
			url : baseUrl+'ui/applicant-status-change',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: statusChangeData
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	return ApplicantService;
}])


.factory('ShopService',['$http','$cordovaFileTransfer',function($http,$cordovaFileTransfer){
	// var baseUrl = 'http://localhost:8000/api/';
	// var baseUrl = 'http://ec2-52-14-198-231.us-east-2.compute.amazonaws.com/api/'	

	var ShopService = {};

	ShopService.getShopNames = function(onSuccess,onError){
		$http({
			method : 'GET',
			url : baseUrl+'ui/get-shop-names',

		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	ShopService.getRetailerShops = function(rId,onSuccess,onError){
		$http({
			method : 'GET',
			url : baseUrl+'ui/retailer-shops/'+rId
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	ShopService.addShop = function(shopData,onSuccess,onError){

		$http({
			method : 'POST',
			url : baseUrl+'shop',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: shopData
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}


	ShopService.uploadShopPhoto = function(imageURI, onSuccess, onError) {
		var options = {
			fileKey: "file",
			fileName: "shop.png",
			chunkedMode: false,
			mimeType: "image/png"
		};
		$cordovaFileTransfer.upload(baseUrl+'ui/upload-shop-image', imageURI, options)
		.then(function(result) {
			onSuccess(result.response);
		}, function(err) {
			onError(err);
		}, function(progress) {

		});
	}

	ShopService.saveShopPhoto = function(shopImageData,onSuccess,onError){

		$http({
			method : 'POST',
			url : baseUrl+'ui/add-shop-image',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: shopImageData
			/* {
				sId: shopId,
				url: 'assets/images/shops/'+shopImageName
			}*/
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});

	}

	ShopService.getShop = function(shopId,onSuccess,onError){

		$http({
			method : 'GET',
			url : baseUrl+'shop/'+shopId
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});

	}

	ShopService.deleteShopImage = function(delShopData,onSuccess,onError){

		$http({
			method : 'POST',
			url : baseUrl+'ui/delete-shop-image',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: delShopData
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError(response);
		});		

	}


	ShopService.updateShop = function(updateShopData,onSuccess,onError) {
		$http({
			method : 'POST',
			url : baseUrl+'shop-update',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: updateShopData
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError(response);
		});	
	}

	ShopService.getAllDepartments = function(onSuccess,onError){
		$http({
			method : 'GET',
			url : baseUrl+'all-department'
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	return ShopService;


}])

.factory('JobService', ['$http',function($http){

	// var baseUrl = 'http://ec2-52-14-198-231.us-east-2.compute.amazonaws.com/api/'	
	// var baseUrl = 'http://locl/api/'	
	var JobService = {};

	JobService.premiumJobs = function(onSuccess,onError){
		$http({
			method : 'GET',
			url : baseUrl+'ui/premium_jobs'
		}).then(function (response) {
			onSuccess(response.data);
		}, function (response) {
			onError(response);
		});
	}

	JobService.getAllTitles = function(onSuccess,onError){
		$http({
			method : 'GET',
			url : baseUrl+'all-job-titles'
		}).then(function (response) {
			onSuccess(response.data);
		}, function (response) {
			onError(response);
		});

	}

	JobService.allJobs = function (onSuccess,onError) {

		$http({
			method : 'GET',
			url : baseUrl+'job'
		}).then(function (response) {
			onSuccess(response.data);
		}, function (response) {
			onError(response);
		});
	}

	JobService.allNewJobs = function (uId,onSuccess,onError) {

		$http({
			method : 'GET',
			url : baseUrl+'ui/all-new-jobs/'+uId
		}).then(function (response) {
			onSuccess(response.data);
		}, function (response) {
			onError(response);
		});
	}


	JobService.getJob =  function(jobId,onSuccess,onError){

		$http({
			method : 'GET',
			url : baseUrl+'job/'+jobId
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});

	}

	JobService.getJobsByShopId =  function(shopId,onSuccess,onError){

		$http({
			method : 'GET',
			url : baseUrl+'ui/shop-jobs/'+shopId
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});

	}

	JobService.store = function(jobData,onSuccess,onError){

		$http({
			method : 'POST',
			url : baseUrl+'job',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: jobData
		}).then(function(response){
			console.log("response"+response)
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});

	}


	JobService.updateJob = function(updateJobData,onSuccess,onError){

		$http({
			method : 'POST',
			url : baseUrl+'job-update',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: updateJobData
		}).then(function(response){
			console.log("response"+response)
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});

	}

	JobService.applyJob = function(applicantsData,onSuccess,onError){
		$http({
			method : 'POST',
			url : baseUrl+'ui/add-applicants',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: applicantsData
		}).then(function(response){
			console.log("response"+response)
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}


	JobService.updateJob = function(jobDetail,onSuccess,onError){

		$http({
			method : 'POST',
			url : baseUrl+'job-update',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: jobDetail
		}).then(function(response){
			console.log("response"+response)
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});

	}

	return JobService;

}])


.factory('SalesmanService', ['$http','$cordovaFileTransfer',function($http,$cordovaFileTransfer){

	// var baseUrl = 'http://localhost:8000/api/';
	// var baseUrl = 'http://ec2-52-14-198-231.us-east-2.compute.amazonaws.com/api/';	
	// var baseUrl = 'http://52.14.198.231/api/';	

	var SalesmanService = {};


	SalesmanService.register = function(salesmanData,onSuccess,onError){

		$http({
			method : 'POST',
			url : baseUrl+'user',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data:salesmanData 
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});

	}

	SalesmanService.checkStatus = function(checkStatusData,onSuccess,onError){
		$http({
			method : 'POST',
			url : baseUrl+'ui/applicant-status-check',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data:checkStatusData 
		}).then(function(response){

			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});

	}		
	

	SalesmanService.allSalesman = function (onSuccess,onError) {

		$http({
			method : 'GET',
			url : baseUrl+'user-salesman'
		}).then(function (response) {
			onSuccess(response.data);
		}, function (response) {
			onError(response);
		});
	}

	SalesmanService.getSalesmanById = function(id,onSuccess,onError){

		
		$http({
			method : 'GET',
			url : baseUrl+'user/'+id
		}).then(function(response){

			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});


	}

	SalesmanService.uploadPhoto = function(imageURI, onSuccess, onError) {
		var options = {
			fileKey: "file",
			fileName: "new.png",
			chunkedMode: false,
			mimeType: "image/png"
		};
		$cordovaFileTransfer.upload(baseUrl+'ui/upload-salesman-image', imageURI, options)
		.then(function(result) {
			onSuccess(result.response);
		}, function(err) {
			onError(err);
		}, function(progress) {

		});
	}


	SalesmanService.salesmanUpdate = function(salesmanUpdateData, onSuccess, onError){

		$http({
			method : 'POST',
			url : baseUrl+'update-user',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data:salesmanUpdateData 
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});


	}

	SalesmanService.saveSalesmanImage = function(saveSalesmanImg, onSuccess, onError)
	{

		$http({
			method : 'POST',
			url : baseUrl+'ui/save-salesman-image',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: saveSalesmanImg 
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});


	}

	SalesmanService.deletePhoto = function(delPhotoData,onSuccess,onError){
		$http({
			method : 'POST',
			url : baseUrl+'ui/delete-salesman-image',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data: delPhotoData 
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});		
	}

	SalesmanService.getSalesmanExperience = function(sId,onSuccess,onError){
		$http({
			method : 'GET',
			url : baseUrl+'salesman-experience/'+sId
		}).then(function(response){

			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	SalesmanService.getExperience = function(eId,onSuccess,onError){
		$http({
			method : 'GET',
			url : baseUrl+'experience/'+eId
		}).then(function(response){

			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	SalesmanService.addExperience = function(experienceData,onSuccess,onError){
		$http({
			method : 'POST',
			url : baseUrl+'add-experience',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data:experienceData 
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	SalesmanService.experienceUpdate = function(experienceUpdateData,onSuccess,onError){
		$http({
			method : 'POST',
			url : baseUrl+'update-experience',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			transformRequest: function(obj) {
				var str = [];
				for(var p in obj)
					str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
				return str.join("&");
			},
			data:experienceUpdateData 
			
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	SalesmanService.getAppliedJobs = function(sId,onSuccess,onError){
		$http({
			method : 'GET',
			url : baseUrl+'ui/salesman-applied-jobs/'+sId
		}).then(function(response){

			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	return SalesmanService;
}])


.factory('SMSservice',['$http', function($http){
	var SMSservice = {};

	SMSservice.checkbalance = function(onSuccess,onError){
		$http({
			method : 'GET',
			url : 'http://www.webpostservice.com/sendsms/checkbalance.php?username=amexsInt&password=435187'
		}).then(function(response){
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	SMSservice.sendsms = function(msg,mobile,onSuccess,onError)
	{
		$http({
			method : 'GET',
			url : 'http://www.webpostservice.com/sendsms/sendsms.php?username=amexsInt&password=435187&type=TEXT&sender=ITLHGO&mobile=' + mobile + '&message=' + msg
		}).then(function(response){
			console.log(response);
			onSuccess(response.data);
		},function(response){
			onError('error occured');
		});
	}

	return SMSservice;
}])

.factory('LocationService', ['$http','$rootScope',function($http,$rootScope){

	var LocationService = {};

	LocationService.initAutocomplete = function(){
		console.log('inside initAutocomplete')
		var input = document.getElementById('pac-input');
		var searchBox = new google.maps.places.SearchBox(input);
		searchBox.addListener('places_changed', function() {
			var places = searchBox.getPlaces();
			if (places.length == 0) {
				return;
			}
			$rootScope.destlat = places[0].geometry.location.lat();
			$rootScope.destlong = places[0].geometry.location.lng();
			var split = document.getElementById('pac-input').value.split(",");
			$rootScope.destaddr = split[0];
			$rootScope.add = document.getElementById('pac-input').value;
			console.log($rootScope.add)
		});
	}

	return LocationService;

}])


.factory("ShopsName",['$http',function($http){

	// var baseUrl = 'http://52.14.198.231/api/';	
	return {
		getSource: function() {
			$http({
				method : 'GET',
				url : baseUrl+'ui/get-shop-names',

			}).then(function(response){

				var shopnames =  response.data;
				console.log(shopnames);

			},function(response){
				alert('error occured');
			});  
		}

	}
}]);


/*.service('LocationService', function($q){
	var autocompleteService = new google.maps.places.AutocompleteService();
	var detailsService = new google.maps.places.PlacesService(document.createElement("input"));
	return {
		searchAddress: function(input) {
			var deferred = $q.defer();

			autocompleteService.getPlacePredictions({
				input: input
			}, function(result, status) {
				if(status == google.maps.places.PlacesServiceStatus.OK){
					console.log(status);
					deferred.resolve(result);
				}else{
					deferred.reject(status)
				}
			});

			return deferred.promise;
		},
		getDetails: function(placeId) {
			var deferred = $q.defer();
			detailsService.getDetails({placeId: placeId}, function(result) {
				deferred.resolve(result);
			});
			return deferred.promise;
		}
	};
})*/