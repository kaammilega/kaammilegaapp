angular.module('app.controllers', ['app.services','ngStorage','ngCordova','ion-google-place','nzToggle','app.directives'])

.controller('allSalesmanCtrl', ['$scope', '$stateParams','$http','$state','SalesmanService','$rootScope','$ionicLoading','$ionicFilterBar',
	function ($scope, $stateParams,$http,$state,SalesmanService,$rootScope,$ionicLoading, $ionicFilterBar) {
		$scope.loading = true;
		$scope.data = {};
		$scope.options = {
			autoplay : 2000,
			loop : true,
			speed: 800
		}
		callAPI();
		function callAPI(){
			SalesmanService.allSalesman(
				function(data){
					console.log(data);
					$scope.loading = false;
					$scope.salesman = data;
					for (var i = 0; i < $scope.salesman.length; i++) {
						$scope.salesman[i].image =$rootScope.baseUrl + $scope.salesman[i].image; 
					}
					console.log($scope.salesman);
					if($scope.salesman.length == 0){$scope.hide = true;}else{
						$scope.hide = false;
					}
				}, function(data){
					$scope.loading = false;
					alert(data);
					alert('Salesman data not received')
				})
		}
		
		$scope.noresults = false;
		$scope.search = false;
		$scope.showFilterBar = function () {
			filterBarInstance = $ionicFilterBar.show({
				items: $scope.salesman,
				cancel: function(){
					$scope.search = false;
				},
				update: function (filteredItems, filterText) {
					console.log(filteredItems)
					if(filteredItems.length == 0){
						$scope.noresults = true;
					}
					$scope.salesman = filteredItems;
					$scope.search = true;
					if (filterText) {
						console.log(filterText);
					}
				}
			});
		};
		$scope.doRefresh =function() {
			callAPI();
			$scope.$broadcast('scroll.refreshComplete');
		}
		$scope.salesmanDetail = function(salesmanId){
			$state.go('applicantDetail',{
				aId: salesmanId,
				vname: 'allSalesman'
			})
		}
	}])


.controller('homeRetailerCtrl', ['$scope', '$stateParams','$http','$state','ApplicantService','$rootScope','$ionicLoading','$ionicFilterBar',
	function ($scope, $stateParams,$http,$state,ApplicantService,$rootScope,$ionicLoading,$ionicFilterBar) {
		$scope.loading = true;
		callAPI();
		function callAPI(){
			ApplicantService.allApplicant($rootScope.id,
				function(data){
					console.log(data);
					if(data == 'noApplicants')
					{
						$scope.noApplicants = true;

					}else{
						$scope.applicants = data;
						for (var i = 0; i < $scope.applicants.length; i++) {
							$scope.applicants[i].image = $rootScope.baseUrl+ $scope.applicants[i].image; 
						}
						$scope.whichapplicant = $state.params.aId;
					}
					$scope.loading = false;
				}, function(data){
					$scope.loading = false;
					console.log(data);
					console.log('Salesman data not received')
				});
		}
		$scope.showFilterBar = function () {
			filterBarInstance = $ionicFilterBar.show({
				items: $scope.applicants,
				update: function (filteredItems, filterText) {
					console.log(filteredItems)
					$scope.applicants = filteredItems;
					if (filterText) {
						console.log(filterText);
					}
				}
			});
		};
		$scope.doRefresh =function() {
			callAPI();
			$scope.$broadcast('scroll.refreshComplete');
		}
		$scope.applicantDetail = function(applicantId){
			$state.go('applicantDetail',{
				aId: applicantId,
				vname: 'homeRetailer'
			})
		}
	}])

.controller('selectedApplicantCtrl', ['$scope', '$stateParams','$http','$state','ApplicantService','$rootScope','$ionicLoading','$ionicFilterBar',
	function ($scope, $stateParams,$http,$state,ApplicantService,$rootScope,$ionicLoading,$ionicFilterBar) {
		$scope.loading = true;
		callAPI();
		function callAPI(){
			ApplicantService.selectedApplicant($rootScope.id,
				function(data){
					console.log(data);
					if(data == 'noApplicants')
					{
						$scope.noApplicants = true;

					}else{
						$scope.applicants = data;
						for (var i = 0; i < $scope.applicants.length; i++) {
							$scope.applicants[i].image = $rootScope.baseUrl+ $scope.applicants[i].image; 
						}
						$scope.whichapplicant = $state.params.aId;
					}
					$scope.loading = false;
				}, function(data){
					$scope.loading = false;
					console.log(data);
					console.log('Salesman data not received')
				});
		}
		$scope.showFilterBar = function () {
			filterBarInstance = $ionicFilterBar.show({
				items: $scope.applicants,
				update: function (filteredItems, filterText) {
					console.log(filteredItems)
					$scope.applicants = filteredItems;
					if (filterText) {
						console.log(filterText);
					}
				}
			});
		};
		$scope.doRefresh =function() {
			callAPI();
			$scope.$broadcast('scroll.refreshComplete');
		}
		$scope.selectedApplicantDetail = function(applicantId){
			$state.go('selectedApplicantDetail',{
				aId: applicantId,
				vname: 'selectedApplicant'
			})
		}
	}])

.controller('rejectedApplicantCtrl', ['$scope', '$stateParams','$http','$state','ApplicantService','$rootScope','$ionicLoading','$ionicFilterBar',
	function ($scope, $stateParams,$http,$state,ApplicantService,$rootScope,$ionicLoading,$ionicFilterBar) {
		$scope.loading = true;

		callAPI();
		function callAPI(){
			ApplicantService.rejectedApplicant($rootScope.id,
				function(data){
					console.log(data);
					if(data == 'noApplicants')
					{
						$scope.noApplicants = true;

					}else{
						$scope.applicants = data;
						for (var i = 0; i < $scope.applicants.length; i++) {
							$scope.applicants[i].image = $rootScope.baseUrl+ $scope.applicants[i].image; 
						}
						$scope.whichapplicant = $state.params.aId;
					}
					$scope.loading = false;
				}, function(data){
					$scope.loading = false;
					console.log(data);
					console.log('Salesman data not received')
				});
		}
		$scope.showFilterBar = function () {
			filterBarInstance = $ionicFilterBar.show({
				items: $scope.applicants,
				update: function (filteredItems, filterText) {
					console.log(filteredItems)
					$scope.applicants = filteredItems;
					if (filterText) {
						console.log(filterText);
					}
				}
			});
		};
		$scope.doRefresh =function() {
			callAPI();
			$scope.$broadcast('scroll.refreshComplete');
		}
		$scope.rejectedApplicantDetail = function(applicantId){
			$state.go('rejectedApplicantDetail',{
				aId: applicantId,
				vname: 'rejectedApplicant'
			})
		}
	}])

.controller('appliedApplicantCtrl', ['$scope', '$stateParams','$http','$state','ApplicantService','$rootScope','$ionicLoading','$ionicFilterBar',
	function ($scope, $stateParams,$http,$state,ApplicantService,$rootScope,$ionicLoading,$ionicFilterBar) {
		$scope.loading = true;

		callAPI();
		function callAPI(){
			ApplicantService.appliedApplicant($rootScope.id,
				function(data){
					console.log(data);
					if(data == 'noApplicants')
					{
						$scope.noApplicants = true;

					}else{
						$scope.applicants = data;
						for (var i = 0; i < $scope.applicants.length; i++) {
							$scope.applicants[i].image = $rootScope.baseUrl+ $scope.applicants[i].image; 
						}
						$scope.whichapplicant = $state.params.aId;
					}
					$scope.loading = false;
				}, function(data){
					$scope.loading = false;
					console.log(data);
					console.log('Salesman data not received')
				});
		}
		$scope.showFilterBar = function () {
			filterBarInstance = $ionicFilterBar.show({
				items: $scope.applicants,
				update: function (filteredItems, filterText) {
					console.log(filteredItems)
					$scope.applicants = filteredItems;
					if (filterText) {
						console.log(filterText);
					}
				}
			});
		};
		$scope.doRefresh =function() {
			callAPI();
			$scope.$broadcast('scroll.refreshComplete');
		}
		$scope.appliedApplicantDetail = function(applicantId){
			$state.go('appliedApplicantDetail',{
				aId: applicantId,
				vname: 'appliedApplicant'
			})
		}
	}])

.controller('applicantDetailCtrl', ['$scope', '$stateParams','$http','$state','ApplicantService','$rootScope','$ionicLoading','$cordovaToast',
	function ($scope, $stateParams,$http,$state,ApplicantService,$rootScope,$ionicLoading,$cordovaToast) {
		$scope.loading = true;
		console.log("shop id: "+$stateParams.sId)
		console.log("job id: "+$stateParams.jId)
		console.log("vname: "+$stateParams.vname)
		$rootScope.jId = $stateParams.jId;
		$rootScope.sId = $stateParams.sId;
		$rootScope.vname = $stateParams.vname;
		if($stateParams.vname == 'allSalesman')
			{$scope.vname = $stateParams.vname;}
		$scope.jobsApplied = false;
		console.log("Applicant id inside applicantDetail: " +$stateParams.aId)
		if($stateParams.aId){
			callAPIs();
		}
		function callAPIs(){
			var applicantData = { aId : $stateParams.aId, rId: $rootScope.id};
			console.log(applicantData);
			ApplicantService.getApplicant(applicantData,
				function(data){
					$scope.loading = false;
					console.log(data);
					$scope.applicant = data;
					$scope.applicant.image = $rootScope.baseUrl+$scope.applicant.image; 
					console.log(data.jobs);
					$scope.jobs = data.jobs;
					if($scope.jobs.length > 0)
					{
						$scope.jobsApplied = true;
					}
					console.log($scope.jobs)
				}, function(data){
					$scope.loading = false;
					alert(data);
					alert('error getting applicant')
				});
		}
		$scope.doRefresh = function(){
			callAPIs();
			$scope.$broadcast('scroll.refreshComplete');
			$state.reload();
		}
		$scope.statusClick = function(status,jobId){
			console.log(jobId+' '+status);
			var uId = $stateParams.aId;
			var jId = jobId
			var statusChangeData = {uId,jId,status};
			ApplicantService.changeStatus(statusChangeData,
				function(data){
					console.log(data);
					$cordovaToast.show(data, 'long', 'center').then(function(success) {}, function(error) {});

				}, function(data){
					$cordovaToast.show('Something went wrong', 'long', 'center').then(function(success) {}, function(error) {});
				})
		}
		$scope.goBack = function(){
			console.log('go back needs to be done depending on where it came from ')
			if($stateParams.vname == 'homeRetailer'){
				$state.go('menu.homeRetailer')
			}
			else if($stateParams.vname == 'jobDetail'){
				$state.go('jobDetail',{
					jId: $stateParams.jId,
					sId: $stateParams.sId
				})
			}
			else if($stateParams.vname == 'shopApplicants'){
				$state.go('shopApplicants',{
					sId: $stateParams.sId
				})
			}
			else if($stateParams.vname == 'allSalesman'){
				$state.go('menu.allSalesman',{
					sId: $stateParams.sId
				})
			}
		}
	}])

.controller('selectedApplicantDetailCtrl', ['$scope', '$stateParams','$http','$state','ApplicantService','$rootScope','$ionicLoading','$cordovaToast',
	function ($scope, $stateParams,$http,$state,ApplicantService,$rootScope,$ionicLoading,$cordovaToast) {
		$scope.loading = true;
		console.log("shop id: "+$stateParams.sId)
		console.log("job id: "+$stateParams.jId)
		console.log("vname: "+$stateParams.vname)
		$rootScope.jId = $stateParams.jId;
		$rootScope.sId = $stateParams.sId;
		$rootScope.vname = $stateParams.vname;
		if($stateParams.vname == 'allSalesman')
			{$scope.vname = $stateParams.vname;}
		$scope.jobsApplied = false;
		console.log("Applicant id inside applicantDetail: " +$stateParams.aId)
		if($stateParams.aId){
			callAPIs();
		}
		function callAPIs(){
			var applicantData = { aId : $stateParams.aId, rId: $rootScope.id};
			console.log(applicantData);
			ApplicantService.getSelectedApplicant(applicantData,
				function(data){
					$scope.loading = false;
					console.log(data);
					$scope.applicant = data;
					$scope.applicant.image = $rootScope.baseUrl+$scope.applicant.image; 
					console.log(data.jobs);
					$scope.jobs = data.jobs;
					if($scope.jobs.length > 0)
					{
						$scope.jobsApplied = true;
					}
					console.log($scope.jobs)
				}, function(data){
					$scope.loading = false;
					alert(data);
					alert('error getting applicant')
				});
		}
		$scope.doRefresh = function(){
			callAPIs();
			$scope.$broadcast('scroll.refreshComplete');
			$state.reload();
		}
		$scope.statusClick = function(status,jobId){
			console.log(jobId+' '+status);
			var uId = $stateParams.aId;
			var jId = jobId
			var statusChangeData = {uId,jId,status};
			ApplicantService.changeStatus(statusChangeData,
				function(data){
					console.log(data);
					$cordovaToast.show(data, 'long', 'center').then(function(success) {}, function(error) {});

				}, function(data){
					$cordovaToast.show('Something went wrong', 'long', 'center').then(function(success) {}, function(error) {});
				})
		}
		$scope.goBack = function(){
			console.log('go back needs to be done depending on where it came from ')
			if($stateParams.vname == 'homeRetailer'){
				$state.go('menu.homeRetailer')
			}
			else if($stateParams.vname == 'jobDetail'){
				$state.go('jobDetail',{
					jId: $stateParams.jId,
					sId: $stateParams.sId
				})
			}
			else if($stateParams.vname == 'shopApplicants'){
				$state.go('shopApplicants',{
					sId: $stateParams.sId
				})
			}
			else if($stateParams.vname == 'allSalesman'){
				$state.go('menu.allSalesman',{
					sId: $stateParams.sId
				})
			}
			else if($stateParams.vname == 'selectedApplicant'){
				$state.go('menu.selectedApplicant')
			}
		}
	}])

.controller('rejectedApplicantDetailCtrl', ['$scope', '$stateParams','$http','$state','ApplicantService','$rootScope','$ionicLoading','$cordovaToast',
	function ($scope, $stateParams,$http,$state,ApplicantService,$rootScope,$ionicLoading,$cordovaToast) {
		$scope.loading = true;
		console.log("shop id: "+$stateParams.sId)
		console.log("job id: "+$stateParams.jId)
		console.log("vname: "+$stateParams.vname)
		$rootScope.jId = $stateParams.jId;
		$rootScope.sId = $stateParams.sId;
		$rootScope.vname = $stateParams.vname;
		if($stateParams.vname == 'allSalesman')
			{$scope.vname = $stateParams.vname;}
		$scope.jobsApplied = false;
		console.log("Applicant id inside applicantDetail: " +$stateParams.aId)
		if($stateParams.aId){
			callAPIs();
		}
		function callAPIs(){
			var applicantData = { aId : $stateParams.aId, rId: $rootScope.id};
			console.log(applicantData);
			ApplicantService.getRejectedApplicant(applicantData,
				function(data){
					$scope.loading = false;
					console.log(data);
					$scope.applicant = data;
					$scope.applicant.image = $rootScope.baseUrl+$scope.applicant.image; 
					console.log(data.jobs);
					$scope.jobs = data.jobs;
					if($scope.jobs.length > 0)
					{
						$scope.jobsApplied = true;
					}
					console.log($scope.jobs)
				}, function(data){
					$scope.loading = false;
					alert(data);
					alert('error getting applicant')
				});
		}
		$scope.doRefresh = function(){
			callAPIs();
			$scope.$broadcast('scroll.refreshComplete');
			$state.reload();
		}
		$scope.statusClick = function(status,jobId){
			console.log(jobId+' '+status);
			var uId = $stateParams.aId;
			var jId = jobId
			var statusChangeData = {uId,jId,status};
			ApplicantService.changeStatus(statusChangeData,
				function(data){
					console.log(data);
					$cordovaToast.show(data, 'long', 'center').then(function(success) {}, function(error) {});

				}, function(data){
					$cordovaToast.show('Something went wrong', 'long', 'center').then(function(success) {}, function(error) {});
				})
		}
		$scope.goBack = function(){
			console.log('go back needs to be done depending on where it came from ')
			if($stateParams.vname == 'homeRetailer'){
				$state.go('menu.homeRetailer')
			}
			else if($stateParams.vname == 'jobDetail'){
				$state.go('jobDetail',{
					jId: $stateParams.jId,
					sId: $stateParams.sId
				})
			}
			else if($stateParams.vname == 'shopApplicants'){
				$state.go('shopApplicants',{
					sId: $stateParams.sId
				})
			}
			else if($stateParams.vname == 'allSalesman'){
				$state.go('menu.allSalesman',{
					sId: $stateParams.sId
				})
			}
			else if($stateParams.vname == 'rejectedApplicant'){
				$state.go('menu.rejectedApplicant')
			}
		}
	}])

.controller('appliedApplicantDetailCtrl', ['$scope', '$stateParams','$http','$state','ApplicantService','$rootScope','$ionicLoading','$cordovaToast',
	function ($scope, $stateParams,$http,$state,ApplicantService,$rootScope,$ionicLoading,$cordovaToast) {
		$scope.loading = true;
		console.log("shop id: "+$stateParams.sId)
		console.log("job id: "+$stateParams.jId)
		console.log("vname: "+$stateParams.vname)
		$rootScope.jId = $stateParams.jId;
		$rootScope.sId = $stateParams.sId;
		$rootScope.vname = $stateParams.vname;
		if($stateParams.vname == 'allSalesman')
			{$scope.vname = $stateParams.vname;}
		$scope.jobsApplied = false;
		console.log("Applicant id inside applicantDetail: " +$stateParams.aId)
		if($stateParams.aId){
			callAPIs();
		}
		function callAPIs(){
			var applicantData = { aId : $stateParams.aId, rId: $rootScope.id};
			console.log(applicantData);
			ApplicantService.getAppliedApplicant(applicantData,
				function(data){
					$scope.loading = false;
					console.log(data);
					$scope.applicant = data;
					$scope.applicant.image = $rootScope.baseUrl+$scope.applicant.image; 
					console.log(data.jobs);
					$scope.jobs = data.jobs;
					if($scope.jobs.length > 0)
					{
						$scope.jobsApplied = true;
					}
					console.log($scope.jobs)
				}, function(data){
					$scope.loading = false;
					alert(data);
					alert('error getting applicant')
				});
		}
		$scope.doRefresh = function(){
			callAPIs();
			$scope.$broadcast('scroll.refreshComplete');
			$state.reload();
		}
		$scope.statusClick = function(status,jobId){
			console.log(jobId+' '+status);
			var uId = $stateParams.aId;
			var jId = jobId
			var statusChangeData = {uId,jId,status};
			ApplicantService.changeStatus(statusChangeData,
				function(data){
					console.log(data);
					$cordovaToast.show(data, 'long', 'center').then(function(success) {}, function(error) {});

				}, function(data){
					$cordovaToast.show('Something went wrong', 'long', 'center').then(function(success) {}, function(error) {});
				})
		}
		$scope.goBack = function(){
			console.log('go back needs to be done depending on where it came from ')
			if($stateParams.vname == 'homeRetailer'){
				$state.go('menu.homeRetailer')
			}
			else if($stateParams.vname == 'jobDetail'){
				$state.go('jobDetail',{
					jId: $stateParams.jId,
					sId: $stateParams.sId
				})
			}
			else if($stateParams.vname == 'shopApplicants'){
				$state.go('shopApplicants',{
					sId: $stateParams.sId
				})
			}
			else if($stateParams.vname == 'allSalesman'){
				$state.go('menu.allSalesman',{
					sId: $stateParams.sId
				})
			}
			else if($stateParams.vname == 'appliedApplicant'){
				$state.go('menu.appliedApplicant')
			}
		}
	}])
	


.controller('homeSalesmanCtrl', ['$scope', '$stateParams','$http','$state','JobService','$ionicLoading','$rootScope','$ionicFilterBar','$cordovaToast',
	function ($scope, $stateParams,$http,$state,JobService,$ionicLoading,$rootScope,$ionicFilterBar,$cordovaToast) {
		$scope.loading = true;
		$scope.options = {
			autoplay : 2000,
			loop : true,
			speed: 800
		}

		// button bar
		$scope.button = {};
		$scope.button.matchingJobs = {};
		$scope.button.otherJobs = {};
		$scope.button.matchingJobs.clicked = true;


		$scope.click = function(button){
			$scope.button.matchingJobs.clicked = false;
			$scope.button.otherJobs.clicked = false;
			
			button.clicked = true;
			console.log($scope.button.matchingJobs.clicked)
			console.log($scope.button.otherJobs.clicked)

		};
		$scope.nomatchedJobs = false;
		var uId = $rootScope.id;
		callAPI();
		function callAPI(){
			JobService.premiumJobs(
				function(data){
					console.log(data);
					$scope.premiumjobs = data;

					JobService.allNewJobs(uId,
						function(data){
							console.log(data);
							console.log(JSON.stringify(data));
							$scope.otherjobs = data.not_matched;
							$scope.matchedjobs = data.matched;
							console.log($scope.matchedjobs.length); 
							if($scope.matchedjobs.length == 0){
								$scope.nomatchedJobs = true;
							}
							console.log($scope.nomatchedJobs);
							$scope.jobs = $scope.premiumjobs.concat($scope.otherjobs).concat($scope.matchedjobs);
							console.log($scope.jobs);
							$scope.loading = false;
						}, function(data){
							$scope.loading = false;
							console.log(data);
							console.log('job data not received')
						})

				},
				function(data){
					alert('error retrieving premium jobs')
				})
		}

		




		$scope.applyJob = function(jId){
				// var jId = jobId;
				var uId = $rootScope.id;
				var applicantsData = {jId,uId};

				JobService.applyJob(applicantsData,
					function(data){
						// $scope.applied = true;
						$cordovaToast.show('Job Applied', 'long', 'center').then(function(success) {}, function(error) {});
						$state.go('allJobDetail',{
							jobId: jId,
							vname: 'homeSalesman'
						});
						
					}, function(data){
						alert('error');
					})

			}



			$scope.noresults = false;
			$scope.search = false;
			$scope.showFilterBar = function () {
				filterBarInstance = $ionicFilterBar.show({
					items: $scope.jobs,
					cancel: function(){
						$scope.search = false;
					},
					update: function (filteredItems, filterText) {
						console.log(filteredItems)
						if(filteredItems.length == 0){
							$scope.noresults = true;
						}
						$scope.jobs = filteredItems;
						$scope.search = true;
						if (filterText) {
							console.log(filterText);
						}
					}
				});
			}



			$scope.jobDetail = function(jobId){
				$state.go('allJobDetail',{
					jobId: jobId,
					vname: 'homeSalesman'
				});
			}
			$scope.doRefresh =function() {
				callAPI();
				$scope.$broadcast('scroll.refreshComplete');
			}
		}])

.controller('salesmanAppliedJobsCtrl', ['$scope', '$stateParams','$http','$state','SalesmanService','$ionicLoading','$rootScope','$ionicFilterBar',
	function ($scope, $stateParams,$http,$state,SalesmanService,$ionicLoading,$rootScope,$ionicFilterBar) {
		$scope.loading = true;
		SalesmanService.getAppliedJobs($rootScope.id,
			function(data){
				$scope.loading = false;
				console.log(data);
				if(data == "no_jobs"){
					$scope.noJobsApplied = true;
				}
				console.log($scope.noJobsApplied);
				// console.log(JSON.stringify(data));
				$scope.appliedJobs = data;
			}, function(data){
				$scope.loading = false;
				alert(data)
				alert('error')
			})
		$scope.showFilterBar = function () {
			filterBarInstance = $ionicFilterBar.show({
				items: $scope.appliedJobs,
				update: function (filteredItems, filterText) {
					console.log(filteredItems)
					$scope.appliedJobs = filteredItems;
					if (filterText) {
						console.log(filterText);
					}
				}
			});
		};
		$scope.doRefresh  =function(){
			SalesmanService.getAppliedJobs($rootScope.id,
				function(data){
					console.log('Salesman Applied Jobs : ' +data);
					console.log('Salesman Applied Jobs in string : ' +JSON.stringify(data));
					$scope.appliedJobs = data;
					$scope.$broadcast('scroll.refreshComplete');
					$state.reload();
				}, function(data){
					alert(data)
					alert('error')
				})
		}
		$scope.jobDetail = function(jobId){
			$state.go('allJobDetail',{
				jobId: jobId,
				vname: 'salesmanAppliedJobs'
			});
		}
	}])


.controller('allJobDetailCtrl', ['$scope', '$stateParams','$http','$state','JobService','$ionicLoading','$rootScope','SalesmanService',
	function ($scope, $stateParams,$http,$state,JobService,$ionicLoading,$rootScope,SalesmanService) {
		$scope.loading = true;
		$rootScope.vname = $state.params.vname;
		if($state.params.jobId){
			callAPIs();
		}
		function callAPIs(){
			console.log('job id inside allJobDetail'+$stateParams.jobId)
			JobService.getJob($state.params.jobId,
				function(data){
					$scope.loading = false;
					console.log(data);
					console.log(JSON.stringify(data));
					$scope.job = data;
					console.log(data[0].shop.images);

					if(!data[0].shop.images){
						$scope.noImages = true;
					}
					$scope.shopUrls = data[0].shop.images;
					for (var i = 0; i < $scope.shopUrls.length; i++) {
						$scope.shopUrls[i] = $rootScope.baseUrl+$scope.shopUrls[i];
					} 
					$scope.singleimg =  data[0].shop.images[0];
					console.log($scope.singleimg);
					console.log("shop images length:- "+data[0].shop.images.length);
					if(data[0].shop.images.length == 1){
						console.log('inside if')
						$scope.singleImage = true;
					}
				}, function(data){
					$scope.loading = false;
					alert('error')
				})
			var uId = $rootScope.id;
			var jId = $state.params.jobId;
			var checkStatusData = {uId,jId};
			SalesmanService.checkStatus(checkStatusData,
				function(data){
					console.log(data);
					$scope.status = data;
					if(data == 'applied' || data=='selected' || data == 'rejected'){
						$scope.applied = true;
					}
				}, function(data){
					alert('error hcecking status')
				})
		}
		$scope.applyJob = function(){
			var jId = $state.params.jobId;
			var uId = $rootScope.id;
			var applicantsData = {jId,uId};

			JobService.applyJob(applicantsData,
				function(data){
					$scope.applied = true;
					$state.reload();
				}, function(data){
					alert('error');
				})

		}
		$scope.doRefresh = function(){
			callAPIs();
			$scope.$broadcast('scroll.refreshComplete');
			$state.reload();
		}

		$scope.goBack = function(){
			if($stateParams.vname == 'homeSalesman')
				$state.go('menu.homeSalesman')
			else if($stateParams.vname == 'salesmanAppliedJobs')
				$state.go('menu.salesmanAppliedJobs')
		}
	}])




.controller('viewJobsCtrl', ['$scope', '$stateParams','JobService','$state','$http','$ionicLoading',
	function ($scope, $stateParams, JobService, $state, $http,$ionicLoading) {

		console.log("shop id inside viewJobs: "+$stateParams.sId);
		$scope.loading = true;

		$scope.noJobs = false;

		if($stateParams.sId){
			$scope.shopId = $stateParams.sId;
			JobService.getJobsByShopId($stateParams.sId,
				function(data){
					// $ionicLoading.hide();
					$scope.loading = false;


					console.log(data);
					console.log(JSON.stringify(data));

					if(data == 'noJobs'){
						console.log('no jobs')
						$scope.noJobs = true;
					}else{
						$scope.jobs = data;}
					}, function(data){
						// $ionicLoading.hide();
						$scope.loading = false;
						console.log(data);
						console.log('error occured');
					})
		}


			/*$scope.addJob = function(){

				$state.go('postJob',{
					vname: 'viewJobs',
					sId : $stateParams.sId
				})
			}*/


			$scope.jDetail = function(jId) {

				console.log('job id: '+jId);
				$state.go('jobDetail',{
					jId: jId,
					sId: $stateParams.sId
				})
			}


			$scope.goBack = function(){
				$state.go('shopDetail',{
					sId: $stateParams.sId
				});
			}

		}])

.controller('jobDetailCtrl', ['$scope', '$stateParams','JobService','$state','$http','$ionicModal','$ionicLoading','ApplicantService','$rootScope','$cordovaToast',
	function ($scope, $stateParams, JobService, $state, $http, $ionicModal, $ionicLoading,ApplicantService, $rootScope,$cordovaToast) {
		$scope.loading = true;
		console.log('job id inside jobDetail'+$stateParams.jId);
		if($stateParams.jId){
			callAPIs();
		}

		function callAPIs(){

			ApplicantService.getApplicantsByJobId($stateParams.jId,
				function(data){
					if(data == "noApplicants"){
						$scope.noApplicants = true;
					}else{
						$scope.applicants = data;
						for (var i = 0; i < $scope.applicants.length; i++) {
							$scope.applicants[i].image = $rootScope.baseUrl+ $scope.applicants[i].image; 
						}
					}
					console.log($scope.applicants)
				}, function(data){
					alert(data);
					alert('error retreiving applicants by job id')
				})
			JobService.getJob($stateParams.jId,
				function(data){
					$scope.loading = false;
					console.log(data);
					// console.log(JSON.stringify(data));
					$scope.job = data[0];
					console.log($scope.job)
					$scope.jobId = data[0].id;
					console.log($scope.jobId);
					$scope.title = data[0].title;
					$scope.requirement = parseInt(data[0].requirement);
					$scope.salary_basis = data[0].salary_basis;

					JobService.getAllTitles(function(data){
						console.log(data);
						console.log($scope.title);
						$scope.titles = data;
						var index = data.findIndex(x => x.job_title==$scope.title);
						console.log(index);
						$scope.selected = $scope.titles[index];
					}, function(data){
						alert('error retreiving job titles data')
					})
					// $scope.job_type = data[0].job_type;
					// $scope.description = data[0].description;
					// $scope.working_hours = data[0].working_hours;
					// $scope.department = data[0].department;
					// $scope.status = data[0].status;
				},function(data){
					$scope.loading = false;
					console.log(data);
					console.log('error occured');
				});




		}



		$scope.doRefresh = function(){
			callAPIs();
			$scope.$broadcast('scroll.refreshComplete');
			$state.reload();
		}



		$scope.updateJob = function(selected,requirement,salary_basis){
			var id = $stateParams.jId;
			console.log(selected);
			var title = selected.id;
			var updateJobData = {id,title,requirement,salary_basis};
			console.log(updateJobData)
			JobService.updateJob(updateJobData,
				function(data){
					$cordovaToast.show('Job Updated', 'long', 'center').then(function(success) {}, function(error) {});
					$state.reload();
					$scope.modal.hide();
				} ,function (data) {
					alert('error updating job data')
					alert(data)
				})
		}
		$scope.applicantDetail = function(applicantId){
			$state.go('applicantDetail',{
				aId: applicantId,
				vname: 'jobDetail',
				jId: $stateParams.jId,
				sId: $stateParams.sId
			})
		}
		$ionicModal.fromTemplateUrl('modal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
		});
		$scope.openModal = function() {
			$scope.modal.show()
		}
		$scope.goBack = function(){
			console.log($stateParams.sId)
			$state.go('viewJobs',{
				sId: $stateParams.sId
			});
		}
	}])



.controller('postJobCtrl', ['$scope', '$stateParams','$state','JobService','$rootScope','$cordovaToast',
	function ($scope, $stateParams,$state,JobService, $rootScope,$cordovaToast) {

		console.log("shop id: " +$stateParams.sId);
		console.log("vname : " +$stateParams.vname);

		$rootScope.sId = $stateParams.sId;
		$rootScope.vname = $stateParams.vname;

		var shopId = $stateParams.sId;

		var rId = $rootScope.id;
		// var rId = 2; //temporary retailer


		JobService.getAllTitles(function(data){
			console.log(data);
			$scope.titles = data; 
			$scope.selected = $scope.titles[0];
		}, function(data){
			alert('error retreiving job titles data')
		})

		$scope.jobSubmit = function(selected,requirement,salary_basis){

			console.log(selected);
			console.log(selected.id);
			var title = selected.id;
			var jobData = {rId,shopId,title,requirement,salary_basis};
			console.log(jobData);
			JobService.store(jobData,
				function(data){
					console.log(JSON.stringify(data));

					$cordovaToast.show('Job Added', 'long', 'center').then(function(success) {}, function(error) {});

					$state.go('viewJobs',{
						sId:  $stateParams.sId
					});
				}, function(data){
					$cordovaToast.show('Something went wrong adding Job', 'long', 'center').then(function(success) {}, function(error) {});
				})


		}

		$scope.goBack = function(){
			if($stateParams.vname == 'viewJobs'){
				$state.go('viewJobs',{
					sId: $stateParams.sId
				});
			}
			else if($stateParams.vname == 'shopDetail'){
				$state.go('shopDetail',{
					sId: $stateParams.sId
				});
			}

		}

	}])

.controller('retailerProfileCtrl', ['$scope', '$stateParams','$rootScope','RetailerService','$state','$ionicModal','$ionicLoading','$cordovaToast',
	function ($scope, $stateParams, $rootScope, RetailerService,$state,$ionicModal,$ionicLoading,$cordovaToast) {

	/*	$ionicLoading.show({
			template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
		});*/
		$scope.loading = true;

		// $rootScope.id = 2;

		RetailerService.getRetailerById($rootScope.id,
			function(data){
				console.log(data);
				console.log('retailer profile' +JSON.stringify(data))
				$scope.retailer = data;
				$scope.retailerDob = Date.parse(data.dob)

				$scope.rId = data.id;
				$scope.name = data.name;
				$scope.email = data.email;
				$scope.dob = new Date(data.dob);
				$scope.mobile = data.mobile;
				$scope.address = data.address;
				$scope.pincode = data.pincode;

				// $ionicLoading.hide();
				$scope.loading = false;

			},function(data){
				// $ionicLoading.hide();
				$scope.loading = false;

				alert(data);
				alert('error retreiving retailer data')
			})


		$scope.retailerUpdate = function(name,email,mobile,address,pincode,dob){

			var id = $scope.rId;

			var retailerUpdateData = {id,name,email,mobile,address,pincode,dob};

			RetailerService.updateRetailer(retailerUpdateData,
				function(data){
					// alert(data);
					$cordovaToast.show('Profile Updated', 'long', 'center').then(function(success) {}, function(error) {});
					// alert('retailer update success')
					$state.reload();
					$scope.modal.hide();

				}, function(data){
					$cordovaToast.show('Error updating profile data', 'long', 'center').then(function(success) {}, function(error) {});
					// alert('error updating retailer data')
					alert(data)
				})

		}

		$ionicModal.fromTemplateUrl('modal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
		});

		$scope.openModal = function() {
			$scope.modal.show()
		}


	}])

.controller('salesmanExperienceCtrl', ['$scope', '$stateParams','$rootScope','SalesmanService','$state','$ionicLoading','LocationService',
	function ($scope, $stateParams, $rootScope, SalesmanService,$state,$ionicLoading,LocationService) {

		$scope.loading = true;

		SalesmanService.getSalesmanExperience($rootScope.id,
			function(data){
				$scope.loading = false;

				// $ionicLoading.hide();
				console.log(data);
				console.log(JSON.stringify(data));

				if(data == 'noExperince'){
					// alert('no Experience found')
					$scope.noExperience = true;
				}else{
					$scope.experiences = data;
				}

			},function(data){
				$scope.loading = false;

				// $ionicLoading.hide();
				console.log('error occured');
			})

		$scope.addExperience = function(){
			$state.go('addExperience',{
				vname: 'salesmanExperience',
				uId : $rootScope.id
			})
		}

		$scope.eDetail = function(eId) {

			$state.go('experienceDetail',{
				eId: eId
			})
		}

	}])


.controller('addExperienceCtrl', ['$scope', '$stateParams','$rootScope','SalesmanService','$state','$ionicLoading','$cordovaToast','LocationService','ShopService','SMSservice',
	function ($scope, $stateParams, $rootScope, SalesmanService,$state,$ionicLoading,$cordovaToast,LocationService,ShopService,SMSservice)
	{
		$scope.currentYear = new Date().getFullYear();
		ShopService.getShopNames(
			function(data){
				console.log(data);
				$scope.shopnames = data; 
			}, function(data){
				alert('error retreiving shop names')
			});



		

		$scope.search = function(typed){
			console.log(typed);

			$scope.searched = true;
		}

		$scope.clicked = function(name){
			delete $scope.searched ;
			console.log(name);
			document.getElementById('selectedShop').value = name;
			// $scope.selectedShop = name;
		}

		ShopService.getAllDepartments
		(
			function(data){
				console.log(data);
				$scope.items = data;
				$scope.items.unshift({
					id:0,
					department: 'Select Department'
				}) 
				// $scope.selected = $scope.items[0];
				$scope.selected = $scope.items[0];

			}, function(data){
				alert('error retreiving departments data')
			});



		console.log($stateParams.vname);
		console.log($stateParams.uId);
		$scope.vname = $stateParams.vname;
		$rootScope.vname = $stateParams.vname;
		var sId = $stateParams.uId;

		$scope.experienceSubmit = function(selectedShop,designation,selected,fromyear,toyear,address,pincode,salary)
		{
			var department = selected.id;
			if(department == 0){
				$scope.errordept = true;
				document.getElementById('depterror').textContent = 'select department';
				return false;
			}
			if(fromyear > toyear){
				console.log('false');
				$scope.error = true;
				document.getElementById('yearerror').textContent = 'From year cannot be greater than toyear';
				return false;
			}
			document.getElementById('yearerror').textContent = '';
			document.getElementById('depterror').textContent = '';
			console.log(selected);
			var company = document.getElementById('selectedShop').value;
			var experienceData = {sId,company,designation,department,fromyear,toyear,address,pincode,salary};
			console.log(experienceData);
			if($scope.vname == 'salesman'){
				$rootScope.experienceData = {company,designation,department,fromyear,toyear,address,pincode,salary};
				console.log($rootScope.salesmanData);
				$rootScope.mobile = $rootScope.salesmanData.mobile;
				console.log($rootScope.mobile);
				$state.go('otp',{
					vname: 'salesman'
				});
			}
			else if ($scope.vname == 'salesmanExperience'){
				SalesmanService.addExperience(experienceData,
					function(data){
						console.log(data);
						$cordovaToast.show('Experience Added', 'long', 'center').then(function(success) {}, function(error) {});
						$state.go('menu.salesmanExperience');

					}, function(data){
						$cordovaToast.show('Error Adding Experience', 'long', 'center').then(function(success) {}, function(error) {});
					})
			}

		}

		$scope.goBack = function(){
			$state.go('menu.salesmanExperience');
		}

	}])


.controller('experienceDetailCtrl', ['$scope', '$stateParams','$rootScope','SalesmanService','$state','$ionicLoading','$ionicModal','ShopService','$cordovaToast',
	function ($scope, $stateParams, $rootScope, SalesmanService,$state,$ionicLoading,$ionicModal,ShopService,$cordovaToast) {

		
		$scope.loading = true;

		if($stateParams.eId){
			SalesmanService.getExperience($stateParams.eId,
				function(data){
					console.log(data);
					$scope.loading = false;

					$scope.company = data[0].company;
					$scope.selectedShop = data[0].company;
					$scope.department = data[0].department;

					ShopService.getAllDepartments
					(
						function(data){
							console.log(data);
							$scope.items = data; 
							var index = data.findIndex(x => x.department==$scope.department);
							$scope.selected = $scope.items[index];
						}, function(data){
							alert('error retreiving departments data')
						});

					ShopService.getShopNames(
						function(data){
							console.log(data);
							$scope.shopnames = data;
							/*var index = data.findIndex(x => x.name==$scope.company);
							$scope.selectedShop = $scope.shopnames[index]; */
						}, function(data){
							alert('error retreiving shop names')
						});

					// $ionicLoading.hide();
					console.log("Experience detail"+data);
					console.log("Experience detail"+JSON.stringify(data));
					$scope.experience = data[0];
					$scope.designation = data[0].designation;


					$scope.salary = data[0].salary;
					$scope.years = data[0].years;
					$scope.fromyear = data[0].from_year;
					$scope.toyear = data[0].to_year;
					$scope.address = data[0].address;
					$scope.pincode = data[0].pincode;
				}, function(data){
					$scope.loading = false;
					alert(data);
					alert('error occured')
				})
		}


		$scope.search = function(typed){
			console.log(typed);

			$scope.searched = true;
		}

		$scope.clicked = function(name){
			delete $scope.searched ;
			console.log(name);
			document.getElementById('selectedShop').value = name;
			$scope.selectedShop = name;
		}

		$scope.experienceUpdate = function(selectedShop,designation,selected,fromyear,toyear,address,pincode,salary){

			if(fromyear > toyear){
				console.log('false');
				$scope.error = true;
				document.getElementById('yearerror').textContent = 'From year cannot be greater than toyear';
				return false;
			}
			document.getElementById('yearerror').textContent = '';

			var years = parseInt(toyear)-parseInt(fromyear);
			console.log("below year");
			console.log(years);
			var company = 	document.getElementById('selectedShop').value;
			var department = selected.id;
			var eId = $stateParams.eId;
			var experienceUpdateData = {eId,company,designation,department,fromyear,toyear,years,address,pincode,salary};
			console.log(experienceUpdateData);
			console.log(JSON.stringify(experienceUpdateData));

			SalesmanService.experienceUpdate(experienceUpdateData,
				function(data){
					$cordovaToast.show('Experience Updated', 'long', 'center').then(function(success) {}, function(error) {});

					$state.reload();
					$scope.modal.hide();
				}, function(data){
					alert(data)
					alert("error")
				})

		}

		$ionicModal.fromTemplateUrl('modal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
		});

		$scope.openModal = function() {
			$scope.modal.show()
		}

		$scope.goBack = function(){
			$state.go('menu.salesmanExperience')
		}

	}])



.controller('retailerShopsCtrl', ['$scope', '$stateParams','$rootScope','ShopService','$state','$ionicLoading',
	function ($scope, $stateParams, $rootScope, ShopService,$state,$ionicLoading) {

		// $rootScope.id = 2; //temporary overriding for testing

		/*$ionicLoading.show({
			template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
		});*/
		$scope.loading = true;

		$scope.noShops = false;

		ShopService.getRetailerShops($rootScope.id,
			function(data){
				$scope.loading = false;

				// $ionicLoading.hide();
				console.log(data);
				console.log(JSON.stringify(data));

				if(data == 'noShops'){
					// alert('no shops found')
					$scope.noShops = true;
				}else{
					$scope.shops = data;
				}

			},function(data){
				$scope.loading = false;

				// $ionicLoading.hide();
				console.log('error occured');
			})

		$scope.sDetail = function(sId) {

			$state.go('shopDetail',{
				sId: sId
			})
		}

	}])

.controller('shopApplicantsCtrl', ['$scope', '$stateParams','$http','$state','ApplicantService','$rootScope','$ionicLoading',
	function ($scope, $stateParams,$http,$state,ApplicantService,$rootScope,$ionicLoading) {
	//Applicants part

	$scope.loading = true;

	ApplicantService.getApplicantsByShopId($stateParams.sId,
		function(data){
			$scope.loading = false;
			if(data.length < 1){
				console.log('inside if')
				$scope.noApplicants = true;
			}else{
				console.log('inside else')
				$scope.applicants = data;
				for (var i = 0; i < $scope.applicants.length; i++) {
					$scope.applicants[i].image = $rootScope.baseUrl+ $scope.applicants[i].image; 
				}
			}
			console.log(data);
			console.log(JSON.stringify(data));
		}, function(data){
			$scope.loading = false;
			alert('error retreiving applicants by shop id')
		})

	$scope.doRefresh = function(){
		ApplicantService.getApplicantsByShopId($stateParams.sId,
			function(data){
				$scope.loading = false;
				if(data.length < 1){
					$scope.noApplicants = true;
				}else{
					$scope.applicants = data;
				}
				console.log(data);
				console.log(JSON.stringify(data));
				$scope.$broadcast('scroll.refreshComplete');
				$state.reload();
			}, function(data){
				$scope.loading = false;
				alert('refersh not complete')
				alert('error retreiving applicants by shop id')
			})
	}


	$scope.applicantDetail = function(applicantId){
		$state.go('applicantDetail',{
			aId: applicantId,
			vname: 'shopApplicants',
			sId: $stateParams.sId
		})
	}


	$scope.goBack = function(){
		$state.go('shopDetail',{
			sId: $stateParams.sId
		});
	}

}])


.controller('shopDetailCtrl', ['$scope', '$stateParams','ShopService','$state','$rootScope','$ionicModal','$ionicActionSheet','$ionicLoading','$ionicPopup','$cordovaToast','ApplicantService',
	function ($scope, $stateParams, ShopService, $state,$rootScope,$ionicModal,$ionicActionSheet,$ionicLoading,$ionicPopup,$cordovaToast,ApplicantService) {

		$scope.options = {
			autoplay : 2000,
			loop : true,
		}

		$rootScope.sId = $stateParams.sId;

		$scope.addShopImage = $rootScope.baseUrl+'assets/images/shops/add-image.png';

		console.log('shop id inside shopDetail: '+$stateParams.sId);
		if($stateParams.sId){
			$scope.loading = true;
			ShopService.getShop($stateParams.sId,
				function(data){
					$scope.department = data.department;

					$scope.loading = false;
					ShopService.getAllDepartments(
						function(data){
							console.log(data);
							$scope.items = data; 
							var index = data.findIndex(x => x.department==$scope.department);
							$scope.selected = $scope.items[index];
						}, function(data){
							alert('error retreiving shop data')
						});
					console.log(data);
					console.log(JSON.stringify(data));
					$scope.shop = data;

					if(data.images){
						$scope.shopImages = [];
						$scope.shopImages = data.images;
						console.log($scope.shopImages)
						for(var i=0;i<$scope.shopImages.length;i++){

							$scope.shopImages[i]=$rootScope.baseUrl+$scope.shopImages[i];
							console.log($scope.shopImages[i])

						}
					}

					$scope.shopId = data.id;
					$scope.name = data.name;
					$scope.contact = data.contact;
					$scope.address = data.address;
					$scope.pincode = data.pincode;
					$scope.established_year = data.established_year;
					$scope.toTime = tConvert(data.toTime);
					$scope.fromTime = tConvert(data.fromTime);
					console.log(data.toTime);
					console.log(data.fromTime);
					// $scope.totime =Date.parse(data.toTime+':00');	
					// $scope.fromtime = Date.parse(data.fromTime+':00');
					$scope.totime = new Date (new Date().toDateString() + ' ' + data.toTime);
					$scope.fromtime = new Date (new Date().toDateString() + ' ' + data.fromTime);

					console.log($scope.totime)	
					console.log($scope.fromtime)
					$scope.department = data.department;

				},function(data){
					$scope.loading = false;
					console.log(data);
					console.log('error occured');
				});



			function tConvert(time) {
				console.log(time);
				time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

				if (time.length > 1) { 
					time = time.slice (1);  
					time[5] = +time[0] < 12 ? 'AM' : 'PM';
					time[0] = +time[0] % 12 || 12; 
				}
				return time.join ('');
			}
		}


		$scope.updateShop = function(name,contact,selected,fromTime,toTime,address,pincode,established_year){

			if(fromTime > toTime){
				console.log('time false')
				$scope.error = true;
				document.getElementById('timeerror').textContent = 'Time Invalid';
				return false;

			}

			document.getElementById('timeerror').textContent = '';


			var id = $scope.shopId;
		// var address = location;
		var shopDepartment = selected.id;
		var updateShopData = {id,name,shopDepartment,contact,fromTime,toTime,address,pincode,established_year};
		console.log(updateShopData);
		ShopService.updateShop(updateShopData,
			function(data){
				console.log('shop updated')
				$scope.modal.hide()
				$state.reload();
			}, function(data){
				alert(data)
				alert('shop not updated')
			});

	}


	$scope.uploadimg = function() {
		$ionicActionSheet.show({
			titleText: 'Upload Profile Picture',
			buttons: [{
				text: '<i class="icon ion-camera positive"></i> Take Picture'
			},
			{
				text: '<i class="icon ion-images positive"></i> Open Gallery'
			},
			],
			destructiveText: 'Close',
			buttonClicked: function(index) {
				if (index === 0) {
					camerapic();
				}

				if (index === 1) {
					gallerypic();
				}
			},
			destructiveButtonClicked: function() {
				return true;
			}
		});
	}

	function camerapic() {

		$ionicLoading.show({
			template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
		});
		navigator.camera.getPicture(success, function(message) {

		}, {
			quality: 50,
			destinationType: navigator.camera.DestinationType.FILE_URI,
			sourceType: navigator.camera.PictureSourceType.camera,
			allowEdit: true,
			targetWidth: 500,
			targetHeight: 500
		});

	}

	function gallerypic() {
		$ionicLoading.show({
			template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
		});
		navigator.camera.getPicture(success, function(message) {
			$ionicLoading.hide();
		}, {
			quality: 50,
			destinationType: navigator.camera.DestinationType.FILE_URI,
			sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
			allowEdit:true,
			targetWidth: 500,
			targetHeight: 500
		});

	}

	function success(imageData) {
		// alert(imageData)
		ShopService.uploadShopPhoto(imageData,
			function(shopImageName){
					// $ionicLoading.hide();
					var sImgUrl = 'assets/images/shops/'+shopImageName;

					var shopImageData = {
						sId: $stateParams.sId,
						url: sImgUrl
					};


					ShopService.saveShopPhoto(shopImageData,
						function(data){
							$ionicLoading.hide();
							$cordovaToast.show('Image added', 'long', 'center').then(function(success) {}, function(error) {});

							$scope.shopImages.push(sImgUrl);
							$state.reload();

						}, function(data){
							$ionicLoading.hide();

							$cordovaToast.show('Something went wrong saving photo', 'long', 'center').then(function(success) {}, function(error) {});
						})

					$state.reload();



				}, function(data){
					$cordovaToast.show('Something went wrong uploading photo', 'long', 'center').then(function(success) {}, function(error) {});
				});

	}



	$scope.deleteimg = function(imgurl) {
		var confirmPopup = $ionicPopup.confirm({
			template: 'Are you sure you want to delete this image?',
			buttons: [{
				text: 'Yes',
				type: 'button-assertive',
				onTap: function(e) {
					delresponse(imgurl);
				}
			},
			{
				text: 'No',
				type: 'button-energized',
				onTap: function(e) {

				}
			}
			]
		});

		confirmPopup.then(function(res) {
			if (res) {} else {}
		});
	}

	function delresponse(imgurl){

		$ionicLoading.show({
			template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
		});
		console.log('image URL:'+imgurl)

		var imguri = imgurl.replace($rootScope.baseUrl,'');
		console.log('image URI:'+imguri)

		var delShopData = {
			sId: $stateParams.sId,
			imgURI: imguri
		};

		console.log(delShopData);

		ShopService.deleteShopImage(delShopData,
			function(data){

				$ionicLoading.hide();
				/*$cordovaToast.show("Image Deleted Successfully!!", 'long', 'center').then(function(success) {}, function(error) {});*/
				/*$state.go($state.current, {}, {
					reload: true
				});*/
				$scope.Imagemodal.hide();
				$state.reload();
			}, function(data){

				$ionicLoading.hide();
				console.log(data);
				$cordovaToast.show('Something went wrong', 'long', 'center').then(function(success) {}, function(error) {});

			})
	}



	ApplicantService.getApplicantsByShopId($stateParams.sId,
		function(data){
			if(data == "noApplicants"){
				$scope.noApplicants = true;
			}else{
				$scope.applicants = data;
			}
			console.log(data);
			console.log(JSON.stringify(data));
		}, function(data){
			alert('error retreiving applicants by shop id')
		})


	$scope.applicantDetail = function(applicantId){
		$state.go('applicantDetail',{
			aId: applicantId,
			vname: 'shopDetail',
			sId: $stateParams.sId
		})
	}



	$ionicModal.fromTemplateUrl('modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});

	$scope.openModal = function() {
		$scope.modal.show()
	}

	$ionicModal.fromTemplateUrl('image-modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(Imagemodal) {
		$scope.Imagemodal = Imagemodal;
	});

	$scope.openImageModal = function(img) {
		$scope.img = img;
		$scope.Imagemodal.show()
	}

	$scope.goBack = function(){
		$state.go('menu.retailerShops');
	}


}])

.controller('addShopCtrl', ['$scope', '$stateParams','$state','ShopService','$ionicLoading','$rootScope','$ionicActionSheet','$cordovaToast','$ionicModal','$ionicPopup','LocationService',
	function ($scope, $stateParams, $state, ShopService, $ionicLoading, $rootScope,$ionicActionSheet,$cordovaToast,$ionicModal,$ionicPopup,LocationService) {
		// $scope.location = {};
		/*LocationService.initAutocomplete();
		$scope.disableTap = function() {
			var container = document.getElementsByClassName('pac-container');
			angular.element(container).attr('data-tap-disabled', 'true');
			var backdrop = document.getElementsByClassName('backdrop');
			angular.element(backdrop).attr('data-tap-disabled', 'true');
			angular.element(container).on("click", function() {
				document.getElementById('pac-input').blur();
			});
		};*/

		$scope.addShopImage = $rootScope.baseUrl+'assets/images/shops/add-image.png';

		ShopService.getAllDepartments(
			function(data){
				console.log(data);
				$scope.items = data; 
				$scope.items.unshift({
					id:0,
					department: 'Select Department'
				}) 
				$scope.selected = $scope.items[0];
			}, function(data){
				alert('error retreiving shop data')
			});



		console.log("retailer id : "+$rootScope.id)
		$scope.srcImage = [];

		$scope.uploadimg = function() {
			$ionicActionSheet.show({
				titleText: 'Upload Profile Picture',
				buttons: [{
					text: '<i class="icon ion-camera positive"></i> Take Picture'
				},
				{
					text: '<i class="icon ion-images positive"></i> Open Gallery'
				},
				],
				destructiveText: 'Close',
				buttonClicked: function(index) {
					if (index === 0) {
						camerapic();
					}

					if (index === 1) {
						gallerypic();
					}
				},
				destructiveButtonClicked: function() {
					return true;
				}
			});
		}

		function camerapic() {

			$ionicLoading.show({
				template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
			});
			navigator.camera.getPicture(success, function(message) {

			}, {
				quality: 50,
				destinationType: navigator.camera.DestinationType.FILE_URI,
				sourceType: navigator.camera.PictureSourceType.camera,
				allowEdit:true,
				targetWidth: 500,
				targetHeight: 500
			});

		}

		function gallerypic() {
			$ionicLoading.show({
				template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
			});
			navigator.camera.getPicture(success, function(message) {
				$ionicLoading.hide();
			}, {
				quality: 50,
				destinationType: navigator.camera.DestinationType.FILE_URI,
				sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
				allowEdit:true,
				targetWidth: 500,
				targetHeight: 500
			});

		}

		function success(imageData) {
			$ionicLoading.hide();
			// alert("srcimage length:= " +$scope.srcImage.length)
			if($scope.srcImage.length < 10){
				$scope.srcImage.push(imageData);
			}
			else if($scope.srcImage.length > 9){
				alert('Cannot add more shop images')
			}

		}



		$scope.shopSubmit = function(name,selected,toTime,fromTime,contact,address,pincode,established_year){
			
			if(selected.id == 0){
				$scope.errordept = true;
				document.getElementById('depterror').textContent = 'select department';
				return false;
			}

			console.log(toTime);
			console.log(fromTime);

			if(fromTime > toTime){
				console.log('time false')
				$scope.error = true;
				document.getElementById('timeerror').textContent = 'Time Invalid';
				return false;

			}

			document.getElementById('timeerror').textContent = '';


			$ionicLoading.show({
				template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
			});
			$scope.shopImg = [];
			if($scope.srcImage.length > 0)
			{
				upload($scope.srcImage);
			}
			else{
				shopRegister();
			}
			function upload(shopImages) {
				ShopService.uploadShopPhoto(shopImages[0],
					function(shopImageName){
						$scope.shopImg.push('assets/images/shops/'+shopImageName);
						shopImages.shift();
						if(shopImages.length != 0){
							upload(shopImages);
						}else if(shopImages.length == 0){
							shopRegister();
						}

					}, function(data){
						$ionicLoading.hide();
						$cordovaToast.show('Something went wrong', 'long', 'center').then(function(success) {}, function(error) {});
					});

			}

			function shopRegister() {

				var rId = $rootScope.id;
				var shop_images = [];
				shop_images = $scope.shopImg;
				var shopDepartment = selected.id;
				var shopData ={rId,name,shopDepartment,toTime,fromTime,contact,address,pincode,established_year,shop_images};
				console.log(shopData);
				ShopService.addShop(shopData,
					function(data){
						$ionicLoading.hide();

						$cordovaToast.show(data, 'long', 'center').then(function(success) {}, function(error) {});
						$state.go('menu.retailerShops')
					}, function(data){
						$ionicLoading.hide();
						alert(data)
						alert('error saving shop')
					})
			}
		}


		$ionicModal.fromTemplateUrl('modal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
		});

		$scope.openModal = function(imageurl) {
			console.log(imageurl)
			$scope.modal.show()
			$scope.imgUrl = imageurl
		}


		$scope.deleteimg = function(imgurl) {
			var confirmPopup = $ionicPopup.confirm({
				template: 'Are you sure you want to delete this image?',
				buttons: [{
					text: 'Yes',
					type: 'button-assertive',
					onTap: function(e) {
						delresponse(imgurl);
					}
				},
				{
					text: 'No',
					type: 'button-energized',
					onTap: function(e) {

					}
				}
				]
			});

			confirmPopup.then(function(res) {
				if (res) {} else {}
			});
		}

		function delresponse(imgurl){

			var index = $scope.srcImage.indexOf(imgurl);
			$scope.srcImage.splice(index, 1);
			$scope.modal.hide();
		}
		$scope.goBack = function(){
			$state.go('menu.retailerShops')
		}



	}])


.controller('menuCtrl', ['$scope', '$stateParams','$ionicPopup','$state','$rootScope','$cordovaFile',
	function ($scope, $stateParams, $ionicPopup, $state, $rootScope, $cordovaFile) {

		/*$rootScope.type = 'salesman';
		$rootScope.id = 69;*/
		// $rootScope.baseUrl = 'http://13.126.220.182/';
		$rootScope.baseUrl = 'http://ec2-13-126-48-166.ap-south-1.compute.amazonaws.com/';
		// $rootScope.baseUrl = 'http://localhost:8000/';	

	// Above are temp data

	console.log($rootScope.type);

	$scope.logout = function(){


		$ionicPopup.confirm({
			title: '<span style="color:black">Logout</span>',
			template: '<span style="color:black">Are you sure you want to logout?</span>',
			okType: 'button-energized'
		}).then(function(res) {
			if(res) {
				delete $rootScope.type;
				delete $rootScope.id;
				$cordovaFile.removeFile(cordova.file.dataDirectory, "kaammilega.txt")
				.then(function(success) {
					$state.go('login')
				}, function(error) {
					alert('error deleting file')
					$state.go('login')
				});
				// $state.go('login');
			} else {
				console.log('You are not sure');
			}});

	}

	// dipesh added
	$scope.groups = [];
    for (var i = 0; i < 1; i++) {
        $scope.groups[i] = {
            name: i,
            items: []
        };
        for (var j = 0; j < 1; j++) {
            $scope.groups[i].items.push(i + '-' + j);
        }
    }


    $scope.toggleGroup = function(group) {
        if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
        } else {
            $scope.shownGroup = group;
        }
    };
    $scope.isGroupShown = function(group) {
        return $scope.shownGroup === group;
    };
	// dipesh added

}])

.controller('otpCtrl',['$scope','$stateParams','$state','$http','$cordovaToast','$ionicLoading','RetailerService','$rootScope','$ionicPopup','SalesmanService','SMSservice',
	function($scope,$stateParams,$state,$http,$cordovaToast,$ionicLoading,RetailerService,$rootScope,$ionicPopup,SalesmanService,SMSservice){
		// console.log($stateParams.vname);
		moveOnMax = function(field, nextFieldID) {
			if (field.value.length == 1) {
				document.getElementById(nextFieldID).focus();
			}
		}
		checkbalance();
		function checkbalance() {
			SMSservice.checkbalance(
				function(data){
					console.log(data);
					if(data < 0){
						$cordovaToast.show('SMS BALANCE END', 'long', 'center').then(function(success) {}, function(error) {});
					}
					else{
						var OTP = Math.floor(1000 + Math.random() * 9000);
						$scope.myotp = OTP;
						console.log(OTP);
						// alert(OTP);
						sendsms(OTP);
					}
				} , function(data){
					$cordovaToast.show('Something went wrong', 'long', 'center').then(function(success) {}, function(error) {});
				});
		}
		function sendsms(OTP) {
			var msg = OTP + " is your account verification number, Enter this in our app to confirm your account";
			console.log($rootScope.mobile);
			SMSservice.sendsms(msg,$rootScope.mobile,
				function(data){
					$ionicLoading.hide();
					console.log(data);
					if (data.includes("SUBMIT_SUCCESS")) {
						

					} else {
						sendsms(OTP);
					}
				}, function(data){
					console.log(data);
					alert('something went wrong sending otp ')
				})

		}


		$scope.validate = function(otp0,otp1,otp2,otp3){
			var otp = otp0.concat(otp1).concat(otp2).concat(otp3);
			console.log(otp);
			if($scope.myotp == otp){
				console.log('otp matched');

				if($stateParams.vname == 'retailer')
				{
					var retailer = $rootScope.retailerData;
					console.log(retailer);
					RetailerService.register(retailer,
						function(data){
							console.log(data);
							$ionicPopup.alert({
								title: "Registered Successfully",
								okType: 'button-energized'
							});
							delete $rootScope.retailerData;
							delete $rootScope.mobile;
							$state.go('login');
						}, function(data){
							$ionicPopup.alert({
								title: 'Error Occured',
								okType: 'button-assertive'
							});
						})	

				}
				else if($stateParams.vname == 'salesman'){
					var salesmanData = $rootScope.salesmanData;
					SalesmanService.register(salesmanData,
						function(data){
							console.log(data);
							var experienceData = $rootScope.experienceData;
							experienceData.sId = data;
							console.log(experienceData);
							SalesmanService.addExperience(experienceData,
								function(data){
									console.log(data);
									$cordovaToast.show('Registered Successfully', 'long', 'center').then(function(success) {}, function(error) {});
									delete $rootScope.retailerData;
									delete $rootScope.mobile;
									delete $rootScope.experienceData
									$state.go('login');

								}, function(data){
									$cordovaToast.show('Something went wrong', 'long', 'center').then(function(success) {}, function(error) {});
								})

							
						}, function(data){
							$ionicLoading.hide();
							$ionicPopup.alert({
								title: 'Error Occured',
								okType: 'button-assertive'
							});
						})

				}
			}
			else{
				$cordovaToast.show('Incorrect OTP entered', 'long', 'center').then(function(success) {}, function(error) {});
				if($stateParams.vname == 'retailer'){
					$state.go('retailer');
				}
				else{
					$state.go('salesman');
				}
			}
		}





	}])

.controller('retailerCtrl', ['$scope', '$stateParams','$state','$http','RetailerService','$ionicPopup','$rootScope','$filter','$ionicLoading','$cordovaToast','SMSservice',
	function ($scope, $stateParams, $state,$http,RetailerService,$ionicPopup,$rootScope,$filter,$ionicLoading,$cordovaToast,SMSservice) {
		$scope.dob = $filter("date")(Date.now(), 'yyyy-MM-dd');
		var dob = document.getElementById('dob');
		dob.value = $scope.dob;
		var type = 'retailer';
		console.log(type);
		$scope.retailerSubmit = function(name,email,password,mobile,address,pincode,gender,dob){
			/*$ionicLoading.show({
				template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
			});*/
			$rootScope.retailerData = {type,name,email,password,mobile,address,pincode,gender,dob};
			$rootScope.mobile = mobile;
			console.log($rootScope.mobile);
			$state.go('otp',{
				vname: 'retailer'
			});
			/*checkbalance();
			function checkbalance() {
				SMSservice.checkbalance(
					function(data){
						console.log(data);
						if(data < 0){
							$cordovaToast.show('SMS BALANCE END', 'long', 'center').then(function(success) {}, function(error) {});
						}
						else{
							var OTP = Math.floor(1000 + Math.random() * 9000);
							$rootScope.myotp = OTP;
							console.log(OTP);
							sendsms(OTP);
						}
					} , function(data){
						$cordovaToast.show('Something went wrong', 'long', 'center').then(function(success) {}, function(error) {});
					});
			}
			function sendsms(OTP) {
				var msg = OTP + " is your account verification number, Enter this in our app to confirm your account";
				SMSservice.sendsms(msg,$scope.mobile,
					function(data){
						$ionicLoading.hide();
						console.log(data);
						if (data.includes("SUBMIT_SUCCESS")) {
							$state.go('otp',{
								vname: 'retailer'
							});
						} else {
							sendsms(OTP);
						}
					}, function(data){
						console.log(data);
						alert('something went wrong sending otp ')
					})

				}*/

			}
		}])



.controller('salesmanProfileCtrl', ['$scope', '$stateParams','$cordovaToast','SalesmanService','$ionicModal','$rootScope','$state','$ionicLoading','$ionicPopup','$cordovaToast','$ionicActionSheet','LocationService',
	function ($scope, $stateParams,$cordovaToast,SalesmanService,$ionicModal,$rootScope,$state,$ionicLoading,$ionicPopup,$cordovaToast,$ionicActionSheet,LocationService) {
		$scope.loading = true;


		SalesmanService.getSalesmanById($rootScope.id,
			function(data){
				$scope.loading = false;
				console.log(data);
				$scope.salesman = data;
				$scope.salesmanDob = Date.parse(data.dob);
				$scope.imageURI = data.image;
				$scope.srcImage = $rootScope.baseUrl+data.image;
				console.log($scope.srcImage)
				var img = $scope.srcImage;
				var split = img.split("/");
				console.log('split: ' +split);
				console.log('split[7]: ='+split[7]);
				$scope.imgName = split[7];
				$scope.sId = data.id;
				$scope.name = data.name;
				$scope.address = data.address;
				$scope.pincode = data.pincode;
				$scope.describe_yourself = data.describe_yourself;
				$scope.salary_basis = data.salary_basis;
				$scope.email = data.email;
				$scope.mobile = data.mobile;
				$scope.dob = new Date(data.dob);
			}, function(data){
				$scope.loading = false;
				// console.log(data);
				console.log('salesman by id data failed to receive');
				$cordovaToast.show('Something went wrong', 'long', 'center').then(function(success) {}, function(error) {});

			})


		$scope.salesmanUpdate = function(name,email,mobile,address,pincode,salary_basis,dob,describe_yourself){

			var id = $scope.sId;
			// var address = $rootScope.add;
			var salesmanUpdateData = {id,name,email,mobile,address,pincode,salary_basis,dob,describe_yourself};

			SalesmanService.salesmanUpdate(salesmanUpdateData,
				function(data){
					$cordovaToast.show('Profile Updated', 'long', 'center').then(function(success) {}, function(error) {});
					$scope.modal.hide();
					$state.reload();
				}, function(data){
					$cordovaToast.show('Something went wrong updating profile', 'long', 'center').then(function(success) {}, function(error) {});
				})

		}


		$scope.uploadImage = function() {
			$ionicActionSheet.show({
				titleText: 'Upload Profile Picture',
				buttons: [{
					text: '<i class="icon ion-camera positive"></i> Take Picture'
				},
				{
					text: '<i class="icon ion-images positive"></i> Open Gallery'
				},
				],
				destructiveText: 'Close',
				buttonClicked: function(index) {
					if (index === 0) {
						camerapic();
					}

					if (index === 1) {
						gallerypic();
					}
				},
				destructiveButtonClicked: function() {
					return true;
				}
			});
		}

		function camerapic() {
			$ionicLoading.show({
				template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
			});
			navigator.camera.getPicture(success, function(message) {

			}, {
				quality: 50,
				destinationType: navigator.camera.DestinationType.FILE_URI,
				sourceType: navigator.camera.PictureSourceType.camera,
				allowEdit:true,
				targetWidth: 500,
				targetHeight: 500
			});

		}

		function gallerypic() {
			$ionicLoading.show({
				template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
			});
			navigator.camera.getPicture(success, function(message) {
				$ionicLoading.hide();
			}, {
				quality: 50,
				destinationType: navigator.camera.DestinationType.FILE_URI,
				sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
				allowEdit:true,
				targetWidth: 500,
				targetHeight: 500
			});

		}


		function success(imageData) {
			// alert('inside success')
			$scope.srcImage = imageData;
			// alert('$scope.srcImage inside success callback:- ' +$scope.srcImage)
			uploadImageData(imageData);
		}

		function uploadImageData(imageData){
			SalesmanService.uploadPhoto(imageData,
				function(imgName){
					var imageURI = 'assets/images/users/salesman/'+imgName;
					var sId = $scope.sId;
					var saveSalesmanPhoto = {sId,imageURI};
					saveImage(saveSalesmanPhoto);
				}, function(data){
					$ionicLoading.hide();
					$cordovaToast.show('Something went wrong', 'long', 'center').then(function(success) {}, function(error) {});
				});
		}
		function saveImage(saveSalesmanImg) {
			SalesmanService.saveSalesmanImage(saveSalesmanImg,
				function(data){

					deleteSalesmanImage($scope.imageURI);

				}, function(data){
					$ionicLoading.hide();
					alert(data);
					alert('error occured')
				});
		}

		function deleteSalesmanImage(imgURI)
		{
			// alert('inside deleteSalesmanImage')
			var sId = $rootScope.id;
			// alert('sId: '+sId)
			var delPhotoData = {sId,imgURI};
			SalesmanService.deletePhoto(delPhotoData,
				function(data){
					$ionicLoading.hide();
				}, function(data){
					$ionicLoading.hide();
					alert(data);
					alert('error occured')
				}
				);
		}

		$scope.data = {};
		$ionicModal.fromTemplateUrl('modal.html', function(modal) {
			$scope.gridModal = modal;
		}, {
			scope: $scope,
			animation: 'slide-in-up'
		});
		// open video modal
		$scope.openModal = function() {
			$scope.data.selected = $scope.srcImage;
			console.log($scope.data.selected);
			$scope.gridModal.show();
		};
		// close video modal
		$scope.closeModal = function() {
			$scope.gridModal.hide();
		};
		//Cleanup the video modal when we're done with it!
		$scope.$on('$destroy', function() {
			$scope.gridModal.remove();
		});

		$ionicModal.fromTemplateUrl('editmodal.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
		});

		$scope.editModal = function() {
			$scope.modal.show()
		}

	}])

.controller('salesmanCtrl', ['$scope', '$stateParams','$state','SalesmanService','$ionicPopup','$ionicLoading','$ionicActionSheet','$cordovaFileTransfer','$http','$cordovaToast','$ionicModal','$filter','$rootScope','LocationService',
	function ($scope, $stateParams,$state,SalesmanService,$ionicPopup,$ionicLoading,$ionicActionSheet,$cordovaFileTransfer,$http,$cordovaToast,$ionicModal,$filter,$rootScope,LocationService) {
		$scope.dob = $filter("date")(Date.now(), 'yyyy-MM-dd');
		var dob = document.getElementById('dob');
		dob.value = $scope.dob;
		$rootScope.baseUrl = 'http://ec2-13-126-48-166.ap-south-1.compute.amazonaws.com/';
		// $rootScope.baseUrl = 'http://13.126.220.182/';
		$scope.salesmanPropic = $rootScope.baseUrl+'assets/images/users/salesman/propic.png';
		console.log($scope.salesmanPropic);
		$scope.uploadImage = function() {
			$ionicActionSheet.show({
				titleText: 'Upload Profile Picture',
				buttons: [{
					text: '<i class="icon ion-camera positive"></i>Take Picture'
				},
				{
					text: '<i class="icon ion-images positive"></i> Open Gallery'
				},
				],
				destructiveText: 'Close',
				buttonClicked: function(index) {
					if (index === 0) {
						camerapic();
					}
					if (index === 1) {
						gallerypic();
					}
				},
				destructiveButtonClicked: function() {
					return true;
				}
			});
		}

		function camerapic() {
			$ionicLoading.show({
				template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
			});
			navigator.camera.getPicture(success, function(message) {

			}, {
				quality: 50,
				destinationType: navigator.camera.DestinationType.FILE_URI,
				sourceType: navigator.camera.PictureSourceType.camera,
				// allowEdit:true,
				targetWidth: 500,
				targetHeight: 500
			});

		}

		function gallerypic() {
			$ionicLoading.show({
				template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
			});
			navigator.camera.getPicture(success, function(message) {
				$ionicLoading.hide();
			}, {
				quality: 50,
				destinationType: navigator.camera.DestinationType.FILE_URI,
				sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
				// allowEdit:true,
				targetWidth: 500,
				targetHeight: 500
			});

		}


		function success(imageData) {
			$ionicLoading.hide();
			$scope.srcImage = imageData;

		}
		$scope.data = {};
		$ionicModal.fromTemplateUrl('modal.html', function(modal) {
			$scope.gridModal = modal;
		}, {
			scope: $scope,
			animation: 'slide-in-up'
		});
		// open video modal
		$scope.openModal = function() {
			$scope.data.selected = $scope.srcImage;
			$scope.gridModal.show();
		};
		// close video modal
		$scope.closeModal = function() {
			$scope.gridModal.hide();
		};
		//Cleanup the video modal when we're done with it!
		$scope.$on('$destroy', function() {
			$scope.gridModal.remove();
		});
		console.log($scope.srcImage);
		$scope.salesmanSubmit = function(name,email,password,mobile,gender,dob,describe_yourself,salary_basis,city,state,pincode,address,address2){
			$ionicLoading.show({
				template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
			});
			if($scope.srcImage)
			{
				SalesmanService.uploadPhoto($scope.srcImage,
					function(imgName){

						$scope.salesmanImg = 'assets/images/users/salesman/'+imgName;
						register();

					}, function(data){
						$ionicLoading.hide();
						$cordovaToast.show('Something went wrong', 'long', 'center').then(function(success) {}, function(error) {});
					});
			}
			else{
				$scope.salesmanImg = 'assets/images/users/salesman/propic.png';
				register();
			}
			function register(){
				$ionicLoading.hide();
				var type= 'salesman';
				var salesmanImg = $scope.salesmanImg;

				$rootScope.salesmanData = {type,name,email,password,mobile,gender,dob,describe_yourself,salary_basis,city,state,pincode,address,address2,salesmanImg};


				$state.go('addExperience',{
					vname: 'salesman'

				});
  		}

  	}

  }])


.controller('loginCtrl', ['$scope', '$stateParams','$state','$location','$ionicPopup','$ionicLoading','loginService','$rootScope','$cordovaFile',
	function ($scope, $stateParams,$state,$location,$ionicPopup,$ionicLoading,loginService,$rootScope,$cordovaFile) {
		// $rootScope.baseUrl = 'http://ec2-52-14-198-231.us-east-2.compute.amazonaws.com/';
		// $rootScope.baseUrl = 'http://13.126.220.182/';
		$scope.formData = {};
		$scope.formSubmit = function(){
			$ionicLoading.show({
				template: '<ion-spinner icon="crescent" class="spinner-energized"></ion-spinner>'
			});
			if($scope.formData.email && $scope.formData.password){
				var email = $scope.formData.email;
				$scope.email = email.toLowerCase();
				// alert("Local storage device :- "+localStorage.getItem("device"));
				var loginData = {
					email: $scope.email,
					password: $scope.formData.password,
					device : localStorage.getItem("device")
				};
				loginService.authenticate(loginData,
					function(data){
						if(data == 'loginSuccess'){
							checkfile($scope.formData.email);
						}
						else if(data == 'IncorrectPassword'){
							$ionicLoading.hide();
							alert('Incorrect password');
						}
						else if(data == 'IncorrectEmail'){
							$ionicLoading.hide();
							alert('Incorrect email');
						}
						else{
							$ionicLoading.hide();
							alert('Invalid credentials')
						}
					}, function(data){
						$ionicLoading.hide();
						alert('error authenticating email');
					})
			}
			else{
				$ionicLoading.hide();
				alert('Fields cannot be empty')
				$state.go('login');
			}
		}

		function checkfile(mail) {
			$cordovaFile.checkFile(cordova.file.dataDirectory, "kaammilega.txt")
			.then(function(success) {
				deletefile(mail);
			}, function(error) {
				createfile(mail);
			});
		}
		function deletefile(mail) {
			$cordovaFile.removeFile(cordova.file.dataDirectory, "kaammilega.txt")
			.then(function(success) {
				createfile(mail);
			}, function(error) {
				createfile(mail);
			});
		}
		function createfile(mail) {
			$cordovaFile.createFile(cordova.file.dataDirectory, "kaammilega.txt", true)
			.then(function(success) {
				writefile(mail);
			}, function(error) {
				createfile(mail);
			});
		}
		function writefile(mail) {
			$cordovaFile.writeExistingFile(cordova.file.dataDirectory, "kaammilega.txt", mail)
			.then(function(success) {
				verifyfile(mail);
			}, function(error) {
				writefile(mail);
			});
		}
		function verifyfile(mail){
			var mailData = {email:mail};
			loginService.verifymail(mailData,
				function(data){
					$ionicLoading.hide();
					var res = data.split(":");
					$rootScope.type = res[0];
					$rootScope.id = res[1];
					if(res[0] == 'retailer')
					{
						$state.go('menu.allSalesman');
					}
					else if(res[0] == 'salesman') {
						$state.go('menu.homeSalesman')
					}
					else{
						alert('Invalid credentials');
					}
				}, function(data){
					$ionicLoading.hide();
					alert('error verifying mail')
					alert(data);
				})
		}
	}])
