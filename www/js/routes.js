angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {


  $stateProvider

  .state('menu', {
    url: '/menu',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl'
  })

  .state('menu.homeRetailer', {
    url: '/homeRetailer',
    views: {
      'menuContent': {
        templateUrl: 'templates/homeRetailer.html',
        controller: 'homeRetailerCtrl'
      }
    }
  })

  .state('menu.selectedApplicant', {
    url: '/selectedApplicant',
    views: {
      'menuContent': {
        templateUrl: 'templates/selectedApplicant.html',
        controller: 'selectedApplicantCtrl'
      }
    }
  })

  .state('menu.rejectedApplicant', {
    url: '/rejectedApplicant',
    views: {
      'menuContent': {
        templateUrl: 'templates/rejectedApplicant.html',
        controller: 'rejectedApplicantCtrl'
      }
    }
  })

  .state('menu.appliedApplicant', {
    url: '/appliedApplicant',
    views: {
      'menuContent': {
        templateUrl: 'templates/appliedApplicant.html',
        controller: 'appliedApplicantCtrl'
      }
    }
  })

  .state('applicantDetail', {
    url: '/applicantDetail',
    params:{
      aId: null,
      vname: null,
      jId: null,
      sId: null
    },
    views: {
      '': {
        templateUrl: 'templates/applicantDetail.html',
        controller: 'applicantDetailCtrl'
      }
    }
  })

  .state('selectedApplicantDetail', {
    url: '/selectedApplicantDetail',
    params:{
      aId: null,
      vname: null,
      jId: null,
      sId: null
    },
    views: {
      '': {
        templateUrl: 'templates/selectedApplicantDetail.html',
        controller: 'selectedApplicantDetailCtrl'
      }
    }
  })

  .state('rejectedApplicantDetail', {
    url: '/rejectedApplicantDetail',
    params:{
      aId: null,
      vname: null,
      jId: null,
      sId: null
    },
    views: {
      '': {
        templateUrl: 'templates/rejectedApplicantDetail.html',
        controller: 'rejectedApplicantDetailCtrl'
      }
    }
  })

  .state('appliedApplicantDetail', {
    url: '/appliedApplicantDetail',
    params:{
      aId: null,
      vname: null,
      jId: null,
      sId: null
    },
    views: {
      '': {
        templateUrl: 'templates/appliedApplicantDetail.html',
        controller: 'appliedApplicantDetailCtrl'
      }
    }
  })

  .state('menu.allSalesman', {
    url: '/allSalesman',
    views: {
      'menuContent': {
        templateUrl: 'templates/allSalesman.html',
        controller: 'allSalesmanCtrl'
      }
    }
  })

  .state('menu.homeSalesman', {
    url: '/homeSalesman',
    views: {
      'menuContent': {
        templateUrl: 'templates/homeSalesman.html',
        controller: 'homeSalesmanCtrl'
      }
    }
  })

  .state('allJobDetail', {
    url: '/allJobDetail',
    params: {
      jobId: null,
      vname: 'homeSalesman'    
    },
    views: {
      '': {
        templateUrl: 'templates/allJobDetail.html',
        controller: 'allJobDetailCtrl'
      }
    }
  })

  .state('menu.salesmanProfile', {
    url: '/salesmanProfile',
    views: {
      'menuContent': {
        templateUrl: 'templates/salesmanProfile.html',
        controller: 'salesmanProfileCtrl'
      }
    }
  })


  .state('menu.salesmanExperience', {
    url: '/salesmanExperience',
    views: {
      'menuContent': {
        templateUrl: 'templates/salesmanExperience.html',
        controller: 'salesmanExperienceCtrl'
      }
    }
  })

  .state('addExperience', {
    url: '/addExperience',
    params:{
      vname: null,
      uId: null
    },
    views: {
      '': {
        templateUrl: 'templates/addExperience.html',
        controller: 'addExperienceCtrl'
      }
    }
  })

  .state('experienceDetail', {
    url: '/experienceDetail',
    params:{
      eId : null
    },
    views: {
      '': {
        templateUrl: 'templates/experienceDetail.html',
        controller: 'experienceDetailCtrl'
      }
    }
  })

  .state('menu.salesmanAppliedJobs', {
    url: '/salesmanAppliedJobs',
    views: {
      'menuContent': {
        templateUrl: 'templates/salesmanAppliedJobs.html',
        controller: 'salesmanAppliedJobsCtrl'
      }
    }
  })

  .state('menu.retailerProfile', {
    url: '/retailerProfile',
    views: {
      'menuContent': {
        templateUrl: 'templates/retailerProfile.html',
        controller: 'retailerProfileCtrl'
      }
    }
  })

  .state('menu.retailerShops', {
    url: '/retailerShops',
    views: {
      'menuContent': {
        templateUrl: 'templates/retailerShops.html',
        controller: 'retailerShopsCtrl'
      }
    }
  })

  .state('shopDetail', {
    url: '/shopDetail',
    params:{
      sId : null
    },
    views: {
      '': {
        templateUrl: 'templates/shopDetail.html',
        controller: 'shopDetailCtrl'
      }
    }
  })

  .state('shopApplicants', {
    url: '/shopApplicants',
    params:{
      sId : null
    },
    views: {
      '': {
        templateUrl: 'templates/shopApplicants.html',
        controller: 'shopApplicantsCtrl'
      }
    }
  })

  .state('addShop', {
    url: '/addShop',
    views: {
      '': {
        templateUrl: 'templates/addShop.html',
        controller: 'addShopCtrl'
      }
    }
  })

  .state('viewJobs', {
    url: '/viewJobs',
    params:{
      sId : null
    },
    views: {
      '': {
        templateUrl: 'templates/viewJobs.html',
        controller: 'viewJobsCtrl'
      }
    }
  })

  .state('jobDetail', {
    url: '/jobDetail',
    params:{
      jId: null,
      sId: null
    },
    views: {
      '': {
        templateUrl: 'templates/jobDetail.html',
        controller: 'jobDetailCtrl'
      }
    }
  })



  .state('postJob', {
    url: '/postJob',
    params:{
      sId : null,
      vname: null
    },
    views: {
      '': {
        templateUrl: 'templates/postJob.html',
        controller: 'postJobCtrl'
      }
    }
  })


  .state('salesman', {
    url: '/salesman',
    templateUrl: 'templates/salesman.html',
    controller: 'salesmanCtrl'
  })

 .state('salesman1', {
    url: '/salesman1',
    templateUrl: 'templates/salesman1.html',
    controller: 'salesmanCtrl'
  })


  .state('retailer', {
    url: '/retailer',
    templateUrl: 'templates/retailer.html',
    controller: 'retailerCtrl'
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('otp', {
    url: '/otp',
    params:{
      vname: null
    },
    templateUrl: 'templates/otp.html',
    controller: 'otpCtrl'
  })


  // $urlRouterProvider.otherwise('menu/salesmanProfile');
  // $urlRouterProvider.otherwise('menu/homeSalesman');


});